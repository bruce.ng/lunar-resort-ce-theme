/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferay.docs.insult.service.persistence;

import com.liferay.docs.insult.NoSuchInsultException;
import com.liferay.docs.insult.model.Insult;
import com.liferay.docs.insult.model.impl.InsultImpl;
import com.liferay.docs.insult.model.impl.InsultModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.security.permission.InlineSQLHelperUtil;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the insult service.
 *
 * <p>
 * Caching information and settings can be found in
 * <code>portal.properties</code>
 * </p>
 *
 * @author joebloggs
 * @see InsultPersistence
 * @see InsultUtil
 * @generated
 */
public class InsultPersistenceImpl extends BasePersistenceImpl<Insult> implements InsultPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 * 
	 * Never modify or reference this class directly. Always use {@link
	 * InsultUtil} to access the insult persistence. Modify
	 * <code>service.xml</code> and rerun ServiceBuilder to regenerate this
	 * class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = InsultImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY + ".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY + ".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(InsultModelImpl.ENTITY_CACHE_ENABLED,
			InsultModelImpl.FINDER_CACHE_ENABLED, InsultImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(InsultModelImpl.ENTITY_CACHE_ENABLED,
			InsultModelImpl.FINDER_CACHE_ENABLED, InsultImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(InsultModelImpl.ENTITY_CACHE_ENABLED, InsultModelImpl.FINDER_CACHE_ENABLED,
			Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID = new FinderPath(InsultModelImpl.ENTITY_CACHE_ENABLED,
			InsultModelImpl.FINDER_CACHE_ENABLED, InsultImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid", new String[] {
					String.class.getName(),
					
					Integer.class.getName(), Integer.class.getName(), OrderByComparator.class.getName() });
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID = new FinderPath(InsultModelImpl.ENTITY_CACHE_ENABLED,
			InsultModelImpl.FINDER_CACHE_ENABLED, InsultImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] { String.class.getName() }, InsultModelImpl.UUID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID = new FinderPath(InsultModelImpl.ENTITY_CACHE_ENABLED,
			InsultModelImpl.FINDER_CACHE_ENABLED, Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] { String.class.getName() });
	
	/**
	 * Returns all the insults where uuid = &#63;.
	 *
	 * @param uuid
	 *            the uuid
	 * @return the matching insults
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public List<Insult> findByUuid(String uuid) throws SystemException {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}
	
	/**
	 * Returns a range of all the insults where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of
	 * <code>end - start</code> instances. <code>start</code> and
	 * <code>end</code> are not primary keys, they are indexes in the result
	 * set. Thus, <code>0</code> refers to the first result in the set. Setting
	 * both <code>start</code> and <code>end</code> to
	 * {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return
	 * the full result set. If <code>orderByComparator</code> is specified, then
	 * the query will include the given ORDER BY logic. If
	 * <code>orderByComparator</code> is absent and pagination is required (
	 * <code>start</code> and <code>end</code> are not
	 * {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the
	 * query will include the default ORDER BY logic from
	 * {@link com.liferay.docs.insult.model.impl.InsultModelImpl}. If both
	 * <code>orderByComparator</code> and pagination are absent, for performance
	 * reasons, the query will not have an ORDER BY clause and the returned
	 * result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid
	 *            the uuid
	 * @param start
	 *            the lower bound of the range of insults
	 * @param end
	 *            the upper bound of the range of insults (not inclusive)
	 * @return the range of matching insults
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public List<Insult> findByUuid(String uuid, int start, int end) throws SystemException {
		return findByUuid(uuid, start, end, null);
	}
	
	/**
	 * Returns an ordered range of all the insults where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of
	 * <code>end - start</code> instances. <code>start</code> and
	 * <code>end</code> are not primary keys, they are indexes in the result
	 * set. Thus, <code>0</code> refers to the first result in the set. Setting
	 * both <code>start</code> and <code>end</code> to
	 * {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return
	 * the full result set. If <code>orderByComparator</code> is specified, then
	 * the query will include the given ORDER BY logic. If
	 * <code>orderByComparator</code> is absent and pagination is required (
	 * <code>start</code> and <code>end</code> are not
	 * {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the
	 * query will include the default ORDER BY logic from
	 * {@link com.liferay.docs.insult.model.impl.InsultModelImpl}. If both
	 * <code>orderByComparator</code> and pagination are absent, for performance
	 * reasons, the query will not have an ORDER BY clause and the returned
	 * result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid
	 *            the uuid
	 * @param start
	 *            the lower bound of the range of insults
	 * @param end
	 *            the upper bound of the range of insults (not inclusive)
	 * @param orderByComparator
	 *            the comparator to order the results by (optionally
	 *            <code>null</code>)
	 * @return the ordered range of matching insults
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public List<Insult> findByUuid(String uuid, int start, int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;
		
		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) && (orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid };
		} else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid, start, end, orderByComparator };
		}
		
		List<Insult> list = (List<Insult>) FinderCacheUtil.getResult(finderPath, finderArgs, this);
		
		if ((list != null) && !list.isEmpty()) {
			for (Insult insult : list) {
				if (!Validator.equals(uuid, insult.getUuid())) {
					list = null;
					
					break;
				}
			}
		}
		
		if (list == null) {
			StringBundler query = null;
			
			if (orderByComparator != null) {
				query = new StringBundler(3 + (orderByComparator.getOrderByFields().length * 3));
			} else {
				query = new StringBundler(3);
			}
			
			query.append(_SQL_SELECT_INSULT_WHERE);
			
			boolean bindUuid = false;
			
			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			} else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			} else {
				bindUuid = true;
				
				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}
			
			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			} else if (pagination) {
				query.append(InsultModelImpl.ORDER_BY_JPQL);
			}
			
			String sql = query.toString();
			
			Session session = null;
			
			try {
				session = openSession();
				
				Query q = session.createQuery(sql);
				
				QueryPos qPos = QueryPos.getInstance(q);
				
				if (bindUuid) {
					qPos.add(uuid);
				}
				
				if (!pagination) {
					list = (List<Insult>) QueryUtil.list(q, getDialect(), start, end, false);
					
					Collections.sort(list);
					
					list = new UnmodifiableList<Insult>(list);
				} else {
					list = (List<Insult>) QueryUtil.list(q, getDialect(), start, end);
				}
				
				cacheResult(list);
				
				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			} catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);
				
				throw processException(e);
			} finally {
				closeSession(session);
			}
		}
		
		return list;
	}
	
	/**
	 * Returns the first insult in the ordered set where uuid = &#63;.
	 *
	 * @param uuid
	 *            the uuid
	 * @param orderByComparator
	 *            the comparator to order the set by (optionally
	 *            <code>null</code>)
	 * @return the first matching insult
	 * @throws com.liferay.docs.insult.NoSuchInsultException
	 *             if a matching insult could not be found
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public Insult findByUuid_First(String uuid, OrderByComparator orderByComparator) throws NoSuchInsultException, SystemException {
		Insult insult = fetchByUuid_First(uuid, orderByComparator);
		
		if (insult != null) {
			return insult;
		}
		
		StringBundler msg = new StringBundler(4);
		
		msg.append(_NO_SUCH_ENTITY_WITH_KEY);
		
		msg.append("uuid=");
		msg.append(uuid);
		
		msg.append(StringPool.CLOSE_CURLY_BRACE);
		
		throw new NoSuchInsultException(msg.toString());
	}
	
	/**
	 * Returns the first insult in the ordered set where uuid = &#63;.
	 *
	 * @param uuid
	 *            the uuid
	 * @param orderByComparator
	 *            the comparator to order the set by (optionally
	 *            <code>null</code>)
	 * @return the first matching insult, or <code>null</code> if a matching
	 *         insult could not be found
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public Insult fetchByUuid_First(String uuid, OrderByComparator orderByComparator) throws SystemException {
		List<Insult> list = findByUuid(uuid, 0, 1, orderByComparator);
		
		if (!list.isEmpty()) {
			return list.get(0);
		}
		
		return null;
	}
	
	/**
	 * Returns the last insult in the ordered set where uuid = &#63;.
	 *
	 * @param uuid
	 *            the uuid
	 * @param orderByComparator
	 *            the comparator to order the set by (optionally
	 *            <code>null</code>)
	 * @return the last matching insult
	 * @throws com.liferay.docs.insult.NoSuchInsultException
	 *             if a matching insult could not be found
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public Insult findByUuid_Last(String uuid, OrderByComparator orderByComparator) throws NoSuchInsultException, SystemException {
		Insult insult = fetchByUuid_Last(uuid, orderByComparator);
		
		if (insult != null) {
			return insult;
		}
		
		StringBundler msg = new StringBundler(4);
		
		msg.append(_NO_SUCH_ENTITY_WITH_KEY);
		
		msg.append("uuid=");
		msg.append(uuid);
		
		msg.append(StringPool.CLOSE_CURLY_BRACE);
		
		throw new NoSuchInsultException(msg.toString());
	}
	
	/**
	 * Returns the last insult in the ordered set where uuid = &#63;.
	 *
	 * @param uuid
	 *            the uuid
	 * @param orderByComparator
	 *            the comparator to order the set by (optionally
	 *            <code>null</code>)
	 * @return the last matching insult, or <code>null</code> if a matching
	 *         insult could not be found
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public Insult fetchByUuid_Last(String uuid, OrderByComparator orderByComparator) throws SystemException {
		int count = countByUuid(uuid);
		
		if (count == 0) {
			return null;
		}
		
		List<Insult> list = findByUuid(uuid, count - 1, count, orderByComparator);
		
		if (!list.isEmpty()) {
			return list.get(0);
		}
		
		return null;
	}
	
	/**
	 * Returns the insults before and after the current insult in the ordered
	 * set where uuid = &#63;.
	 *
	 * @param insultId
	 *            the primary key of the current insult
	 * @param uuid
	 *            the uuid
	 * @param orderByComparator
	 *            the comparator to order the set by (optionally
	 *            <code>null</code>)
	 * @return the previous, current, and next insult
	 * @throws com.liferay.docs.insult.NoSuchInsultException
	 *             if a insult with the primary key could not be found
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public Insult[] findByUuid_PrevAndNext(long insultId, String uuid, OrderByComparator orderByComparator) throws NoSuchInsultException,
			SystemException {
		Insult insult = findByPrimaryKey(insultId);
		
		Session session = null;
		
		try {
			session = openSession();
			
			Insult[] array = new InsultImpl[3];
			
			array[0] = getByUuid_PrevAndNext(session, insult, uuid, orderByComparator, true);
			
			array[1] = insult;
			
			array[2] = getByUuid_PrevAndNext(session, insult, uuid, orderByComparator, false);
			
			return array;
		} catch (Exception e) {
			throw processException(e);
		} finally {
			closeSession(session);
		}
	}
	
	protected Insult getByUuid_PrevAndNext(Session session, Insult insult, String uuid, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;
		
		if (orderByComparator != null) {
			query = new StringBundler(6 + (orderByComparator.getOrderByFields().length * 6));
		} else {
			query = new StringBundler(3);
		}
		
		query.append(_SQL_SELECT_INSULT_WHERE);
		
		boolean bindUuid = false;
		
		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_UUID_1);
		} else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		} else {
			bindUuid = true;
			
			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}
		
		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();
			
			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}
			
			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);
				
				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					} else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				} else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					} else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}
			
			query.append(ORDER_BY_CLAUSE);
			
			String[] orderByFields = orderByComparator.getOrderByFields();
			
			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);
				
				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					} else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				} else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					} else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		} else {
			query.append(InsultModelImpl.ORDER_BY_JPQL);
		}
		
		String sql = query.toString();
		
		Query q = session.createQuery(sql);
		
		q.setFirstResult(0);
		q.setMaxResults(2);
		
		QueryPos qPos = QueryPos.getInstance(q);
		
		if (bindUuid) {
			qPos.add(uuid);
		}
		
		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(insult);
			
			for (Object value : values) {
				qPos.add(value);
			}
		}
		
		List<Insult> list = q.list();
		
		if (list.size() == 2) {
			return list.get(1);
		} else {
			return null;
		}
	}
	
	/**
	 * Removes all the insults where uuid = &#63; from the database.
	 *
	 * @param uuid
	 *            the uuid
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public void removeByUuid(String uuid) throws SystemException {
		for (Insult insult : findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(insult);
		}
	}
	
	/**
	 * Returns the number of insults where uuid = &#63;.
	 *
	 * @param uuid
	 *            the uuid
	 * @return the number of matching insults
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public int countByUuid(String uuid) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID;
		
		Object[] finderArgs = new Object[] { uuid };
		
		Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs, this);
		
		if (count == null) {
			StringBundler query = new StringBundler(2);
			
			query.append(_SQL_COUNT_INSULT_WHERE);
			
			boolean bindUuid = false;
			
			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			} else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			} else {
				bindUuid = true;
				
				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}
			
			String sql = query.toString();
			
			Session session = null;
			
			try {
				session = openSession();
				
				Query q = session.createQuery(sql);
				
				QueryPos qPos = QueryPos.getInstance(q);
				
				if (bindUuid) {
					qPos.add(uuid);
				}
				
				count = (Long) q.uniqueResult();
				
				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			} catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);
				
				throw processException(e);
			} finally {
				closeSession(session);
			}
		}
		
		return count.intValue();
	}
	
	private static final String _FINDER_COLUMN_UUID_UUID_1 = "insult.uuid IS NULL";
	private static final String _FINDER_COLUMN_UUID_UUID_2 = "insult.uuid = ?";
	private static final String _FINDER_COLUMN_UUID_UUID_3 = "(insult.uuid IS NULL OR insult.uuid = '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_UUID_G = new FinderPath(InsultModelImpl.ENTITY_CACHE_ENABLED,
			InsultModelImpl.FINDER_CACHE_ENABLED, InsultImpl.class, FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G", new String[] { String.class.getName(),
					Long.class.getName() }, InsultModelImpl.UUID_COLUMN_BITMASK | InsultModelImpl.GROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_G = new FinderPath(InsultModelImpl.ENTITY_CACHE_ENABLED,
			InsultModelImpl.FINDER_CACHE_ENABLED, Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G", new String[] {
					String.class.getName(), Long.class.getName() });
	
	/**
	 * Returns the insult where uuid = &#63; and groupId = &#63; or throws a
	 * {@link com.liferay.docs.insult.NoSuchInsultException} if it could not be
	 * found.
	 *
	 * @param uuid
	 *            the uuid
	 * @param groupId
	 *            the group ID
	 * @return the matching insult
	 * @throws com.liferay.docs.insult.NoSuchInsultException
	 *             if a matching insult could not be found
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public Insult findByUUID_G(String uuid, long groupId) throws NoSuchInsultException, SystemException {
		Insult insult = fetchByUUID_G(uuid, groupId);
		
		if (insult == null) {
			StringBundler msg = new StringBundler(6);
			
			msg.append(_NO_SUCH_ENTITY_WITH_KEY);
			
			msg.append("uuid=");
			msg.append(uuid);
			
			msg.append(", groupId=");
			msg.append(groupId);
			
			msg.append(StringPool.CLOSE_CURLY_BRACE);
			
			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}
			
			throw new NoSuchInsultException(msg.toString());
		}
		
		return insult;
	}
	
	/**
	 * Returns the insult where uuid = &#63; and groupId = &#63; or returns
	 * <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid
	 *            the uuid
	 * @param groupId
	 *            the group ID
	 * @return the matching insult, or <code>null</code> if a matching insult
	 *         could not be found
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public Insult fetchByUUID_G(String uuid, long groupId) throws SystemException {
		return fetchByUUID_G(uuid, groupId, true);
	}
	
	/**
	 * Returns the insult where uuid = &#63; and groupId = &#63; or returns
	 * <code>null</code> if it could not be found, optionally using the finder
	 * cache.
	 *
	 * @param uuid
	 *            the uuid
	 * @param groupId
	 *            the group ID
	 * @param retrieveFromCache
	 *            whether to use the finder cache
	 * @return the matching insult, or <code>null</code> if a matching insult
	 *         could not be found
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public Insult fetchByUUID_G(String uuid, long groupId, boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { uuid, groupId };
		
		Object result = null;
		
		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_UUID_G, finderArgs, this);
		}
		
		if (result instanceof Insult) {
			Insult insult = (Insult) result;
			
			if (!Validator.equals(uuid, insult.getUuid()) || (groupId != insult.getGroupId())) {
				result = null;
			}
		}
		
		if (result == null) {
			StringBundler query = new StringBundler(4);
			
			query.append(_SQL_SELECT_INSULT_WHERE);
			
			boolean bindUuid = false;
			
			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			} else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			} else {
				bindUuid = true;
				
				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}
			
			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);
			
			String sql = query.toString();
			
			Session session = null;
			
			try {
				session = openSession();
				
				Query q = session.createQuery(sql);
				
				QueryPos qPos = QueryPos.getInstance(q);
				
				if (bindUuid) {
					qPos.add(uuid);
				}
				
				qPos.add(groupId);
				
				List<Insult> list = q.list();
				
				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G, finderArgs, list);
				} else {
					Insult insult = list.get(0);
					
					result = insult;
					
					cacheResult(insult);
					
					if ((insult.getUuid() == null) || !insult.getUuid().equals(uuid) || (insult.getGroupId() != groupId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G, finderArgs, insult);
					}
				}
			} catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_UUID_G, finderArgs);
				
				throw processException(e);
			} finally {
				closeSession(session);
			}
		}
		
		if (result instanceof List<?>) {
			return null;
		} else {
			return (Insult) result;
		}
	}
	
	/**
	 * Removes the insult where uuid = &#63; and groupId = &#63; from the
	 * database.
	 *
	 * @param uuid
	 *            the uuid
	 * @param groupId
	 *            the group ID
	 * @return the insult that was removed
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public Insult removeByUUID_G(String uuid, long groupId) throws NoSuchInsultException, SystemException {
		Insult insult = findByUUID_G(uuid, groupId);
		
		return remove(insult);
	}
	
	/**
	 * Returns the number of insults where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid
	 *            the uuid
	 * @param groupId
	 *            the group ID
	 * @return the number of matching insults
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_G;
		
		Object[] finderArgs = new Object[] { uuid, groupId };
		
		Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs, this);
		
		if (count == null) {
			StringBundler query = new StringBundler(3);
			
			query.append(_SQL_COUNT_INSULT_WHERE);
			
			boolean bindUuid = false;
			
			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			} else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			} else {
				bindUuid = true;
				
				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}
			
			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);
			
			String sql = query.toString();
			
			Session session = null;
			
			try {
				session = openSession();
				
				Query q = session.createQuery(sql);
				
				QueryPos qPos = QueryPos.getInstance(q);
				
				if (bindUuid) {
					qPos.add(uuid);
				}
				
				qPos.add(groupId);
				
				count = (Long) q.uniqueResult();
				
				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			} catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);
				
				throw processException(e);
			} finally {
				closeSession(session);
			}
		}
		
		return count.intValue();
	}
	
	private static final String _FINDER_COLUMN_UUID_G_UUID_1 = "insult.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_2 = "insult.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_3 = "(insult.uuid IS NULL OR insult.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 = "insult.groupId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C = new FinderPath(InsultModelImpl.ENTITY_CACHE_ENABLED,
			InsultModelImpl.FINDER_CACHE_ENABLED, InsultImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C", new String[] {
					String.class.getName(), Long.class.getName(),
					
					Integer.class.getName(), Integer.class.getName(), OrderByComparator.class.getName() });
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C = new FinderPath(InsultModelImpl.ENTITY_CACHE_ENABLED,
			InsultModelImpl.FINDER_CACHE_ENABLED, InsultImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C", new String[] {
					String.class.getName(), Long.class.getName() }, InsultModelImpl.UUID_COLUMN_BITMASK | InsultModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_C = new FinderPath(InsultModelImpl.ENTITY_CACHE_ENABLED,
			InsultModelImpl.FINDER_CACHE_ENABLED, Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C", new String[] {
					String.class.getName(), Long.class.getName() });
	
	/**
	 * Returns all the insults where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid
	 *            the uuid
	 * @param companyId
	 *            the company ID
	 * @return the matching insults
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public List<Insult> findByUuid_C(String uuid, long companyId) throws SystemException {
		return findByUuid_C(uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}
	
	/**
	 * Returns a range of all the insults where uuid = &#63; and companyId =
	 * &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of
	 * <code>end - start</code> instances. <code>start</code> and
	 * <code>end</code> are not primary keys, they are indexes in the result
	 * set. Thus, <code>0</code> refers to the first result in the set. Setting
	 * both <code>start</code> and <code>end</code> to
	 * {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return
	 * the full result set. If <code>orderByComparator</code> is specified, then
	 * the query will include the given ORDER BY logic. If
	 * <code>orderByComparator</code> is absent and pagination is required (
	 * <code>start</code> and <code>end</code> are not
	 * {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the
	 * query will include the default ORDER BY logic from
	 * {@link com.liferay.docs.insult.model.impl.InsultModelImpl}. If both
	 * <code>orderByComparator</code> and pagination are absent, for performance
	 * reasons, the query will not have an ORDER BY clause and the returned
	 * result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid
	 *            the uuid
	 * @param companyId
	 *            the company ID
	 * @param start
	 *            the lower bound of the range of insults
	 * @param end
	 *            the upper bound of the range of insults (not inclusive)
	 * @return the range of matching insults
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public List<Insult> findByUuid_C(String uuid, long companyId, int start, int end) throws SystemException {
		return findByUuid_C(uuid, companyId, start, end, null);
	}
	
	/**
	 * Returns an ordered range of all the insults where uuid = &#63; and
	 * companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of
	 * <code>end - start</code> instances. <code>start</code> and
	 * <code>end</code> are not primary keys, they are indexes in the result
	 * set. Thus, <code>0</code> refers to the first result in the set. Setting
	 * both <code>start</code> and <code>end</code> to
	 * {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return
	 * the full result set. If <code>orderByComparator</code> is specified, then
	 * the query will include the given ORDER BY logic. If
	 * <code>orderByComparator</code> is absent and pagination is required (
	 * <code>start</code> and <code>end</code> are not
	 * {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the
	 * query will include the default ORDER BY logic from
	 * {@link com.liferay.docs.insult.model.impl.InsultModelImpl}. If both
	 * <code>orderByComparator</code> and pagination are absent, for performance
	 * reasons, the query will not have an ORDER BY clause and the returned
	 * result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid
	 *            the uuid
	 * @param companyId
	 *            the company ID
	 * @param start
	 *            the lower bound of the range of insults
	 * @param end
	 *            the upper bound of the range of insults (not inclusive)
	 * @param orderByComparator
	 *            the comparator to order the results by (optionally
	 *            <code>null</code>)
	 * @return the ordered range of matching insults
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public List<Insult> findByUuid_C(String uuid, long companyId, int start, int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;
		
		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) && (orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] { uuid, companyId };
		} else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] { uuid, companyId,
			
			start, end, orderByComparator };
		}
		
		List<Insult> list = (List<Insult>) FinderCacheUtil.getResult(finderPath, finderArgs, this);
		
		if ((list != null) && !list.isEmpty()) {
			for (Insult insult : list) {
				if (!Validator.equals(uuid, insult.getUuid()) || (companyId != insult.getCompanyId())) {
					list = null;
					
					break;
				}
			}
		}
		
		if (list == null) {
			StringBundler query = null;
			
			if (orderByComparator != null) {
				query = new StringBundler(4 + (orderByComparator.getOrderByFields().length * 3));
			} else {
				query = new StringBundler(4);
			}
			
			query.append(_SQL_SELECT_INSULT_WHERE);
			
			boolean bindUuid = false;
			
			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			} else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			} else {
				bindUuid = true;
				
				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}
			
			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);
			
			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			} else if (pagination) {
				query.append(InsultModelImpl.ORDER_BY_JPQL);
			}
			
			String sql = query.toString();
			
			Session session = null;
			
			try {
				session = openSession();
				
				Query q = session.createQuery(sql);
				
				QueryPos qPos = QueryPos.getInstance(q);
				
				if (bindUuid) {
					qPos.add(uuid);
				}
				
				qPos.add(companyId);
				
				if (!pagination) {
					list = (List<Insult>) QueryUtil.list(q, getDialect(), start, end, false);
					
					Collections.sort(list);
					
					list = new UnmodifiableList<Insult>(list);
				} else {
					list = (List<Insult>) QueryUtil.list(q, getDialect(), start, end);
				}
				
				cacheResult(list);
				
				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			} catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);
				
				throw processException(e);
			} finally {
				closeSession(session);
			}
		}
		
		return list;
	}
	
	/**
	 * Returns the first insult in the ordered set where uuid = &#63; and
	 * companyId = &#63;.
	 *
	 * @param uuid
	 *            the uuid
	 * @param companyId
	 *            the company ID
	 * @param orderByComparator
	 *            the comparator to order the set by (optionally
	 *            <code>null</code>)
	 * @return the first matching insult
	 * @throws com.liferay.docs.insult.NoSuchInsultException
	 *             if a matching insult could not be found
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public Insult findByUuid_C_First(String uuid, long companyId, OrderByComparator orderByComparator) throws NoSuchInsultException, SystemException {
		Insult insult = fetchByUuid_C_First(uuid, companyId, orderByComparator);
		
		if (insult != null) {
			return insult;
		}
		
		StringBundler msg = new StringBundler(6);
		
		msg.append(_NO_SUCH_ENTITY_WITH_KEY);
		
		msg.append("uuid=");
		msg.append(uuid);
		
		msg.append(", companyId=");
		msg.append(companyId);
		
		msg.append(StringPool.CLOSE_CURLY_BRACE);
		
		throw new NoSuchInsultException(msg.toString());
	}
	
	/**
	 * Returns the first insult in the ordered set where uuid = &#63; and
	 * companyId = &#63;.
	 *
	 * @param uuid
	 *            the uuid
	 * @param companyId
	 *            the company ID
	 * @param orderByComparator
	 *            the comparator to order the set by (optionally
	 *            <code>null</code>)
	 * @return the first matching insult, or <code>null</code> if a matching
	 *         insult could not be found
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public Insult fetchByUuid_C_First(String uuid, long companyId, OrderByComparator orderByComparator) throws SystemException {
		List<Insult> list = findByUuid_C(uuid, companyId, 0, 1, orderByComparator);
		
		if (!list.isEmpty()) {
			return list.get(0);
		}
		
		return null;
	}
	
	/**
	 * Returns the last insult in the ordered set where uuid = &#63; and
	 * companyId = &#63;.
	 *
	 * @param uuid
	 *            the uuid
	 * @param companyId
	 *            the company ID
	 * @param orderByComparator
	 *            the comparator to order the set by (optionally
	 *            <code>null</code>)
	 * @return the last matching insult
	 * @throws com.liferay.docs.insult.NoSuchInsultException
	 *             if a matching insult could not be found
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public Insult findByUuid_C_Last(String uuid, long companyId, OrderByComparator orderByComparator) throws NoSuchInsultException, SystemException {
		Insult insult = fetchByUuid_C_Last(uuid, companyId, orderByComparator);
		
		if (insult != null) {
			return insult;
		}
		
		StringBundler msg = new StringBundler(6);
		
		msg.append(_NO_SUCH_ENTITY_WITH_KEY);
		
		msg.append("uuid=");
		msg.append(uuid);
		
		msg.append(", companyId=");
		msg.append(companyId);
		
		msg.append(StringPool.CLOSE_CURLY_BRACE);
		
		throw new NoSuchInsultException(msg.toString());
	}
	
	/**
	 * Returns the last insult in the ordered set where uuid = &#63; and
	 * companyId = &#63;.
	 *
	 * @param uuid
	 *            the uuid
	 * @param companyId
	 *            the company ID
	 * @param orderByComparator
	 *            the comparator to order the set by (optionally
	 *            <code>null</code>)
	 * @return the last matching insult, or <code>null</code> if a matching
	 *         insult could not be found
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public Insult fetchByUuid_C_Last(String uuid, long companyId, OrderByComparator orderByComparator) throws SystemException {
		int count = countByUuid_C(uuid, companyId);
		
		if (count == 0) {
			return null;
		}
		
		List<Insult> list = findByUuid_C(uuid, companyId, count - 1, count, orderByComparator);
		
		if (!list.isEmpty()) {
			return list.get(0);
		}
		
		return null;
	}
	
	/**
	 * Returns the insults before and after the current insult in the ordered
	 * set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param insultId
	 *            the primary key of the current insult
	 * @param uuid
	 *            the uuid
	 * @param companyId
	 *            the company ID
	 * @param orderByComparator
	 *            the comparator to order the set by (optionally
	 *            <code>null</code>)
	 * @return the previous, current, and next insult
	 * @throws com.liferay.docs.insult.NoSuchInsultException
	 *             if a insult with the primary key could not be found
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public Insult[] findByUuid_C_PrevAndNext(long insultId, String uuid, long companyId, OrderByComparator orderByComparator)
			throws NoSuchInsultException, SystemException {
		Insult insult = findByPrimaryKey(insultId);
		
		Session session = null;
		
		try {
			session = openSession();
			
			Insult[] array = new InsultImpl[3];
			
			array[0] = getByUuid_C_PrevAndNext(session, insult, uuid, companyId, orderByComparator, true);
			
			array[1] = insult;
			
			array[2] = getByUuid_C_PrevAndNext(session, insult, uuid, companyId, orderByComparator, false);
			
			return array;
		} catch (Exception e) {
			throw processException(e);
		} finally {
			closeSession(session);
		}
	}
	
	protected Insult getByUuid_C_PrevAndNext(Session session, Insult insult, String uuid, long companyId, OrderByComparator orderByComparator,
			boolean previous) {
		StringBundler query = null;
		
		if (orderByComparator != null) {
			query = new StringBundler(6 + (orderByComparator.getOrderByFields().length * 6));
		} else {
			query = new StringBundler(3);
		}
		
		query.append(_SQL_SELECT_INSULT_WHERE);
		
		boolean bindUuid = false;
		
		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_1);
		} else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		} else {
			bindUuid = true;
			
			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}
		
		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);
		
		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();
			
			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}
			
			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);
				
				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					} else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				} else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					} else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}
			
			query.append(ORDER_BY_CLAUSE);
			
			String[] orderByFields = orderByComparator.getOrderByFields();
			
			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);
				
				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					} else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				} else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					} else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		} else {
			query.append(InsultModelImpl.ORDER_BY_JPQL);
		}
		
		String sql = query.toString();
		
		Query q = session.createQuery(sql);
		
		q.setFirstResult(0);
		q.setMaxResults(2);
		
		QueryPos qPos = QueryPos.getInstance(q);
		
		if (bindUuid) {
			qPos.add(uuid);
		}
		
		qPos.add(companyId);
		
		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(insult);
			
			for (Object value : values) {
				qPos.add(value);
			}
		}
		
		List<Insult> list = q.list();
		
		if (list.size() == 2) {
			return list.get(1);
		} else {
			return null;
		}
	}
	
	/**
	 * Removes all the insults where uuid = &#63; and companyId = &#63; from the
	 * database.
	 *
	 * @param uuid
	 *            the uuid
	 * @param companyId
	 *            the company ID
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) throws SystemException {
		for (Insult insult : findByUuid_C(uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(insult);
		}
	}
	
	/**
	 * Returns the number of insults where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid
	 *            the uuid
	 * @param companyId
	 *            the company ID
	 * @return the number of matching insults
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_C;
		
		Object[] finderArgs = new Object[] { uuid, companyId };
		
		Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs, this);
		
		if (count == null) {
			StringBundler query = new StringBundler(3);
			
			query.append(_SQL_COUNT_INSULT_WHERE);
			
			boolean bindUuid = false;
			
			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			} else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			} else {
				bindUuid = true;
				
				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}
			
			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);
			
			String sql = query.toString();
			
			Session session = null;
			
			try {
				session = openSession();
				
				Query q = session.createQuery(sql);
				
				QueryPos qPos = QueryPos.getInstance(q);
				
				if (bindUuid) {
					qPos.add(uuid);
				}
				
				qPos.add(companyId);
				
				count = (Long) q.uniqueResult();
				
				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			} catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);
				
				throw processException(e);
			} finally {
				closeSession(session);
			}
		}
		
		return count.intValue();
	}
	
	private static final String _FINDER_COLUMN_UUID_C_UUID_1 = "insult.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_2 = "insult.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_3 = "(insult.uuid IS NULL OR insult.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 = "insult.companyId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_GROUPID = new FinderPath(InsultModelImpl.ENTITY_CACHE_ENABLED,
			InsultModelImpl.FINDER_CACHE_ENABLED, InsultImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByGroupId", new String[] {
					Long.class.getName(),
					
					Integer.class.getName(), Integer.class.getName(), OrderByComparator.class.getName() });
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID = new FinderPath(InsultModelImpl.ENTITY_CACHE_ENABLED,
			InsultModelImpl.FINDER_CACHE_ENABLED, InsultImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByGroupId",
			new String[] { Long.class.getName() }, InsultModelImpl.GROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_GROUPID = new FinderPath(InsultModelImpl.ENTITY_CACHE_ENABLED,
			InsultModelImpl.FINDER_CACHE_ENABLED, Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByGroupId",
			new String[] { Long.class.getName() });
	
	/**
	 * Returns all the insults where groupId = &#63;.
	 *
	 * @param groupId
	 *            the group ID
	 * @return the matching insults
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public List<Insult> findByGroupId(long groupId) throws SystemException {
		return findByGroupId(groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}
	
	/**
	 * Returns a range of all the insults where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of
	 * <code>end - start</code> instances. <code>start</code> and
	 * <code>end</code> are not primary keys, they are indexes in the result
	 * set. Thus, <code>0</code> refers to the first result in the set. Setting
	 * both <code>start</code> and <code>end</code> to
	 * {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return
	 * the full result set. If <code>orderByComparator</code> is specified, then
	 * the query will include the given ORDER BY logic. If
	 * <code>orderByComparator</code> is absent and pagination is required (
	 * <code>start</code> and <code>end</code> are not
	 * {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the
	 * query will include the default ORDER BY logic from
	 * {@link com.liferay.docs.insult.model.impl.InsultModelImpl}. If both
	 * <code>orderByComparator</code> and pagination are absent, for performance
	 * reasons, the query will not have an ORDER BY clause and the returned
	 * result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId
	 *            the group ID
	 * @param start
	 *            the lower bound of the range of insults
	 * @param end
	 *            the upper bound of the range of insults (not inclusive)
	 * @return the range of matching insults
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public List<Insult> findByGroupId(long groupId, int start, int end) throws SystemException {
		return findByGroupId(groupId, start, end, null);
	}
	
	/**
	 * Returns an ordered range of all the insults where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of
	 * <code>end - start</code> instances. <code>start</code> and
	 * <code>end</code> are not primary keys, they are indexes in the result
	 * set. Thus, <code>0</code> refers to the first result in the set. Setting
	 * both <code>start</code> and <code>end</code> to
	 * {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return
	 * the full result set. If <code>orderByComparator</code> is specified, then
	 * the query will include the given ORDER BY logic. If
	 * <code>orderByComparator</code> is absent and pagination is required (
	 * <code>start</code> and <code>end</code> are not
	 * {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the
	 * query will include the default ORDER BY logic from
	 * {@link com.liferay.docs.insult.model.impl.InsultModelImpl}. If both
	 * <code>orderByComparator</code> and pagination are absent, for performance
	 * reasons, the query will not have an ORDER BY clause and the returned
	 * result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId
	 *            the group ID
	 * @param start
	 *            the lower bound of the range of insults
	 * @param end
	 *            the upper bound of the range of insults (not inclusive)
	 * @param orderByComparator
	 *            the comparator to order the results by (optionally
	 *            <code>null</code>)
	 * @return the ordered range of matching insults
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public List<Insult> findByGroupId(long groupId, int start, int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;
		
		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) && (orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID;
			finderArgs = new Object[] { groupId };
		} else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_GROUPID;
			finderArgs = new Object[] { groupId, start, end, orderByComparator };
		}
		
		List<Insult> list = (List<Insult>) FinderCacheUtil.getResult(finderPath, finderArgs, this);
		
		if ((list != null) && !list.isEmpty()) {
			for (Insult insult : list) {
				if ((groupId != insult.getGroupId())) {
					list = null;
					
					break;
				}
			}
		}
		
		if (list == null) {
			StringBundler query = null;
			
			if (orderByComparator != null) {
				query = new StringBundler(3 + (orderByComparator.getOrderByFields().length * 3));
			} else {
				query = new StringBundler(3);
			}
			
			query.append(_SQL_SELECT_INSULT_WHERE);
			
			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);
			
			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			} else if (pagination) {
				query.append(InsultModelImpl.ORDER_BY_JPQL);
			}
			
			String sql = query.toString();
			
			Session session = null;
			
			try {
				session = openSession();
				
				Query q = session.createQuery(sql);
				
				QueryPos qPos = QueryPos.getInstance(q);
				
				qPos.add(groupId);
				
				if (!pagination) {
					list = (List<Insult>) QueryUtil.list(q, getDialect(), start, end, false);
					
					Collections.sort(list);
					
					list = new UnmodifiableList<Insult>(list);
				} else {
					list = (List<Insult>) QueryUtil.list(q, getDialect(), start, end);
				}
				
				cacheResult(list);
				
				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			} catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);
				
				throw processException(e);
			} finally {
				closeSession(session);
			}
		}
		
		return list;
	}
	
	/**
	 * Returns the first insult in the ordered set where groupId = &#63;.
	 *
	 * @param groupId
	 *            the group ID
	 * @param orderByComparator
	 *            the comparator to order the set by (optionally
	 *            <code>null</code>)
	 * @return the first matching insult
	 * @throws com.liferay.docs.insult.NoSuchInsultException
	 *             if a matching insult could not be found
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public Insult findByGroupId_First(long groupId, OrderByComparator orderByComparator) throws NoSuchInsultException, SystemException {
		Insult insult = fetchByGroupId_First(groupId, orderByComparator);
		
		if (insult != null) {
			return insult;
		}
		
		StringBundler msg = new StringBundler(4);
		
		msg.append(_NO_SUCH_ENTITY_WITH_KEY);
		
		msg.append("groupId=");
		msg.append(groupId);
		
		msg.append(StringPool.CLOSE_CURLY_BRACE);
		
		throw new NoSuchInsultException(msg.toString());
	}
	
	/**
	 * Returns the first insult in the ordered set where groupId = &#63;.
	 *
	 * @param groupId
	 *            the group ID
	 * @param orderByComparator
	 *            the comparator to order the set by (optionally
	 *            <code>null</code>)
	 * @return the first matching insult, or <code>null</code> if a matching
	 *         insult could not be found
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public Insult fetchByGroupId_First(long groupId, OrderByComparator orderByComparator) throws SystemException {
		List<Insult> list = findByGroupId(groupId, 0, 1, orderByComparator);
		
		if (!list.isEmpty()) {
			return list.get(0);
		}
		
		return null;
	}
	
	/**
	 * Returns the last insult in the ordered set where groupId = &#63;.
	 *
	 * @param groupId
	 *            the group ID
	 * @param orderByComparator
	 *            the comparator to order the set by (optionally
	 *            <code>null</code>)
	 * @return the last matching insult
	 * @throws com.liferay.docs.insult.NoSuchInsultException
	 *             if a matching insult could not be found
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public Insult findByGroupId_Last(long groupId, OrderByComparator orderByComparator) throws NoSuchInsultException, SystemException {
		Insult insult = fetchByGroupId_Last(groupId, orderByComparator);
		
		if (insult != null) {
			return insult;
		}
		
		StringBundler msg = new StringBundler(4);
		
		msg.append(_NO_SUCH_ENTITY_WITH_KEY);
		
		msg.append("groupId=");
		msg.append(groupId);
		
		msg.append(StringPool.CLOSE_CURLY_BRACE);
		
		throw new NoSuchInsultException(msg.toString());
	}
	
	/**
	 * Returns the last insult in the ordered set where groupId = &#63;.
	 *
	 * @param groupId
	 *            the group ID
	 * @param orderByComparator
	 *            the comparator to order the set by (optionally
	 *            <code>null</code>)
	 * @return the last matching insult, or <code>null</code> if a matching
	 *         insult could not be found
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public Insult fetchByGroupId_Last(long groupId, OrderByComparator orderByComparator) throws SystemException {
		int count = countByGroupId(groupId);
		
		if (count == 0) {
			return null;
		}
		
		List<Insult> list = findByGroupId(groupId, count - 1, count, orderByComparator);
		
		if (!list.isEmpty()) {
			return list.get(0);
		}
		
		return null;
	}
	
	/**
	 * Returns the insults before and after the current insult in the ordered
	 * set where groupId = &#63;.
	 *
	 * @param insultId
	 *            the primary key of the current insult
	 * @param groupId
	 *            the group ID
	 * @param orderByComparator
	 *            the comparator to order the set by (optionally
	 *            <code>null</code>)
	 * @return the previous, current, and next insult
	 * @throws com.liferay.docs.insult.NoSuchInsultException
	 *             if a insult with the primary key could not be found
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public Insult[] findByGroupId_PrevAndNext(long insultId, long groupId, OrderByComparator orderByComparator) throws NoSuchInsultException,
			SystemException {
		Insult insult = findByPrimaryKey(insultId);
		
		Session session = null;
		
		try {
			session = openSession();
			
			Insult[] array = new InsultImpl[3];
			
			array[0] = getByGroupId_PrevAndNext(session, insult, groupId, orderByComparator, true);
			
			array[1] = insult;
			
			array[2] = getByGroupId_PrevAndNext(session, insult, groupId, orderByComparator, false);
			
			return array;
		} catch (Exception e) {
			throw processException(e);
		} finally {
			closeSession(session);
		}
	}
	
	protected Insult getByGroupId_PrevAndNext(Session session, Insult insult, long groupId, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;
		
		if (orderByComparator != null) {
			query = new StringBundler(6 + (orderByComparator.getOrderByFields().length * 6));
		} else {
			query = new StringBundler(3);
		}
		
		query.append(_SQL_SELECT_INSULT_WHERE);
		
		query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);
		
		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();
			
			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}
			
			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);
				
				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					} else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				} else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					} else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}
			
			query.append(ORDER_BY_CLAUSE);
			
			String[] orderByFields = orderByComparator.getOrderByFields();
			
			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);
				
				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					} else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				} else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					} else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		} else {
			query.append(InsultModelImpl.ORDER_BY_JPQL);
		}
		
		String sql = query.toString();
		
		Query q = session.createQuery(sql);
		
		q.setFirstResult(0);
		q.setMaxResults(2);
		
		QueryPos qPos = QueryPos.getInstance(q);
		
		qPos.add(groupId);
		
		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(insult);
			
			for (Object value : values) {
				qPos.add(value);
			}
		}
		
		List<Insult> list = q.list();
		
		if (list.size() == 2) {
			return list.get(1);
		} else {
			return null;
		}
	}
	
	/**
	 * Returns all the insults that the user has permission to view where
	 * groupId = &#63;.
	 *
	 * @param groupId
	 *            the group ID
	 * @return the matching insults that the user has permission to view
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public List<Insult> filterFindByGroupId(long groupId) throws SystemException {
		return filterFindByGroupId(groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}
	
	/**
	 * Returns a range of all the insults that the user has permission to view
	 * where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of
	 * <code>end - start</code> instances. <code>start</code> and
	 * <code>end</code> are not primary keys, they are indexes in the result
	 * set. Thus, <code>0</code> refers to the first result in the set. Setting
	 * both <code>start</code> and <code>end</code> to
	 * {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return
	 * the full result set. If <code>orderByComparator</code> is specified, then
	 * the query will include the given ORDER BY logic. If
	 * <code>orderByComparator</code> is absent and pagination is required (
	 * <code>start</code> and <code>end</code> are not
	 * {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the
	 * query will include the default ORDER BY logic from
	 * {@link com.liferay.docs.insult.model.impl.InsultModelImpl}. If both
	 * <code>orderByComparator</code> and pagination are absent, for performance
	 * reasons, the query will not have an ORDER BY clause and the returned
	 * result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId
	 *            the group ID
	 * @param start
	 *            the lower bound of the range of insults
	 * @param end
	 *            the upper bound of the range of insults (not inclusive)
	 * @return the range of matching insults that the user has permission to
	 *         view
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public List<Insult> filterFindByGroupId(long groupId, int start, int end) throws SystemException {
		return filterFindByGroupId(groupId, start, end, null);
	}
	
	/**
	 * Returns an ordered range of all the insults that the user has permissions
	 * to view where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of
	 * <code>end - start</code> instances. <code>start</code> and
	 * <code>end</code> are not primary keys, they are indexes in the result
	 * set. Thus, <code>0</code> refers to the first result in the set. Setting
	 * both <code>start</code> and <code>end</code> to
	 * {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return
	 * the full result set. If <code>orderByComparator</code> is specified, then
	 * the query will include the given ORDER BY logic. If
	 * <code>orderByComparator</code> is absent and pagination is required (
	 * <code>start</code> and <code>end</code> are not
	 * {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the
	 * query will include the default ORDER BY logic from
	 * {@link com.liferay.docs.insult.model.impl.InsultModelImpl}. If both
	 * <code>orderByComparator</code> and pagination are absent, for performance
	 * reasons, the query will not have an ORDER BY clause and the returned
	 * result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId
	 *            the group ID
	 * @param start
	 *            the lower bound of the range of insults
	 * @param end
	 *            the upper bound of the range of insults (not inclusive)
	 * @param orderByComparator
	 *            the comparator to order the results by (optionally
	 *            <code>null</code>)
	 * @return the ordered range of matching insults that the user has
	 *         permission to view
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public List<Insult> filterFindByGroupId(long groupId, int start, int end, OrderByComparator orderByComparator) throws SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return findByGroupId(groupId, start, end, orderByComparator);
		}
		
		StringBundler query = null;
		
		if (orderByComparator != null) {
			query = new StringBundler(3 + (orderByComparator.getOrderByFields().length * 3));
		} else {
			query = new StringBundler(3);
		}
		
		if (getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_INSULT_WHERE);
		} else {
			query.append(_FILTER_SQL_SELECT_INSULT_NO_INLINE_DISTINCT_WHERE_1);
		}
		
		query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);
		
		if (!getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_INSULT_NO_INLINE_DISTINCT_WHERE_2);
		}
		
		if (orderByComparator != null) {
			if (getDB().isSupportsInlineDistinct()) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS, orderByComparator, true);
			} else {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_TABLE, orderByComparator, true);
			}
		} else {
			if (getDB().isSupportsInlineDistinct()) {
				query.append(InsultModelImpl.ORDER_BY_JPQL);
			} else {
				query.append(InsultModelImpl.ORDER_BY_SQL);
			}
		}
		
		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(), Insult.class.getName(), _FILTER_ENTITY_TABLE_FILTER_PK_COLUMN,
				groupId);
		
		Session session = null;
		
		try {
			session = openSession();
			
			SQLQuery q = session.createSQLQuery(sql);
			
			if (getDB().isSupportsInlineDistinct()) {
				q.addEntity(_FILTER_ENTITY_ALIAS, InsultImpl.class);
			} else {
				q.addEntity(_FILTER_ENTITY_TABLE, InsultImpl.class);
			}
			
			QueryPos qPos = QueryPos.getInstance(q);
			
			qPos.add(groupId);
			
			return (List<Insult>) QueryUtil.list(q, getDialect(), start, end);
		} catch (Exception e) {
			throw processException(e);
		} finally {
			closeSession(session);
		}
	}
	
	/**
	 * Returns the insults before and after the current insult in the ordered
	 * set of insults that the user has permission to view where groupId =
	 * &#63;.
	 *
	 * @param insultId
	 *            the primary key of the current insult
	 * @param groupId
	 *            the group ID
	 * @param orderByComparator
	 *            the comparator to order the set by (optionally
	 *            <code>null</code>)
	 * @return the previous, current, and next insult
	 * @throws com.liferay.docs.insult.NoSuchInsultException
	 *             if a insult with the primary key could not be found
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public Insult[] filterFindByGroupId_PrevAndNext(long insultId, long groupId, OrderByComparator orderByComparator) throws NoSuchInsultException,
			SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return findByGroupId_PrevAndNext(insultId, groupId, orderByComparator);
		}
		
		Insult insult = findByPrimaryKey(insultId);
		
		Session session = null;
		
		try {
			session = openSession();
			
			Insult[] array = new InsultImpl[3];
			
			array[0] = filterGetByGroupId_PrevAndNext(session, insult, groupId, orderByComparator, true);
			
			array[1] = insult;
			
			array[2] = filterGetByGroupId_PrevAndNext(session, insult, groupId, orderByComparator, false);
			
			return array;
		} catch (Exception e) {
			throw processException(e);
		} finally {
			closeSession(session);
		}
	}
	
	protected Insult filterGetByGroupId_PrevAndNext(Session session, Insult insult, long groupId, OrderByComparator orderByComparator,
			boolean previous) {
		StringBundler query = null;
		
		if (orderByComparator != null) {
			query = new StringBundler(6 + (orderByComparator.getOrderByFields().length * 6));
		} else {
			query = new StringBundler(3);
		}
		
		if (getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_INSULT_WHERE);
		} else {
			query.append(_FILTER_SQL_SELECT_INSULT_NO_INLINE_DISTINCT_WHERE_1);
		}
		
		query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);
		
		if (!getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_INSULT_NO_INLINE_DISTINCT_WHERE_2);
		}
		
		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();
			
			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}
			
			for (int i = 0; i < orderByConditionFields.length; i++) {
				if (getDB().isSupportsInlineDistinct()) {
					query.append(_ORDER_BY_ENTITY_ALIAS);
				} else {
					query.append(_ORDER_BY_ENTITY_TABLE);
				}
				
				query.append(orderByConditionFields[i]);
				
				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					} else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				} else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					} else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}
			
			query.append(ORDER_BY_CLAUSE);
			
			String[] orderByFields = orderByComparator.getOrderByFields();
			
			for (int i = 0; i < orderByFields.length; i++) {
				if (getDB().isSupportsInlineDistinct()) {
					query.append(_ORDER_BY_ENTITY_ALIAS);
				} else {
					query.append(_ORDER_BY_ENTITY_TABLE);
				}
				
				query.append(orderByFields[i]);
				
				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					} else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				} else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					} else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		} else {
			if (getDB().isSupportsInlineDistinct()) {
				query.append(InsultModelImpl.ORDER_BY_JPQL);
			} else {
				query.append(InsultModelImpl.ORDER_BY_SQL);
			}
		}
		
		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(), Insult.class.getName(), _FILTER_ENTITY_TABLE_FILTER_PK_COLUMN,
				groupId);
		
		SQLQuery q = session.createSQLQuery(sql);
		
		q.setFirstResult(0);
		q.setMaxResults(2);
		
		if (getDB().isSupportsInlineDistinct()) {
			q.addEntity(_FILTER_ENTITY_ALIAS, InsultImpl.class);
		} else {
			q.addEntity(_FILTER_ENTITY_TABLE, InsultImpl.class);
		}
		
		QueryPos qPos = QueryPos.getInstance(q);
		
		qPos.add(groupId);
		
		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(insult);
			
			for (Object value : values) {
				qPos.add(value);
			}
		}
		
		List<Insult> list = q.list();
		
		if (list.size() == 2) {
			return list.get(1);
		} else {
			return null;
		}
	}
	
	/**
	 * Removes all the insults where groupId = &#63; from the database.
	 *
	 * @param groupId
	 *            the group ID
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public void removeByGroupId(long groupId) throws SystemException {
		for (Insult insult : findByGroupId(groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(insult);
		}
	}
	
	/**
	 * Returns the number of insults where groupId = &#63;.
	 *
	 * @param groupId
	 *            the group ID
	 * @return the number of matching insults
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public int countByGroupId(long groupId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_GROUPID;
		
		Object[] finderArgs = new Object[] { groupId };
		
		Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs, this);
		
		if (count == null) {
			StringBundler query = new StringBundler(2);
			
			query.append(_SQL_COUNT_INSULT_WHERE);
			
			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);
			
			String sql = query.toString();
			
			Session session = null;
			
			try {
				session = openSession();
				
				Query q = session.createQuery(sql);
				
				QueryPos qPos = QueryPos.getInstance(q);
				
				qPos.add(groupId);
				
				count = (Long) q.uniqueResult();
				
				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			} catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);
				
				throw processException(e);
			} finally {
				closeSession(session);
			}
		}
		
		return count.intValue();
	}
	
	/**
	 * Returns the number of insults that the user has permission to view where
	 * groupId = &#63;.
	 *
	 * @param groupId
	 *            the group ID
	 * @return the number of matching insults that the user has permission to
	 *         view
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public int filterCountByGroupId(long groupId) throws SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return countByGroupId(groupId);
		}
		
		StringBundler query = new StringBundler(2);
		
		query.append(_FILTER_SQL_COUNT_INSULT_WHERE);
		
		query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);
		
		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(), Insult.class.getName(), _FILTER_ENTITY_TABLE_FILTER_PK_COLUMN,
				groupId);
		
		Session session = null;
		
		try {
			session = openSession();
			
			SQLQuery q = session.createSQLQuery(sql);
			
			q.addScalar(COUNT_COLUMN_NAME, com.liferay.portal.kernel.dao.orm.Type.LONG);
			
			QueryPos qPos = QueryPos.getInstance(q);
			
			qPos.add(groupId);
			
			Long count = (Long) q.uniqueResult();
			
			return count.intValue();
		} catch (Exception e) {
			throw processException(e);
		} finally {
			closeSession(session);
		}
	}
	
	private static final String _FINDER_COLUMN_GROUPID_GROUPID_2 = "insult.groupId = ?";
	
	public InsultPersistenceImpl() {
		setModelClass(Insult.class);
	}
	
	/**
	 * Caches the insult in the entity cache if it is enabled.
	 *
	 * @param insult
	 *            the insult
	 */
	@Override
	public void cacheResult(Insult insult) {
		EntityCacheUtil.putResult(InsultModelImpl.ENTITY_CACHE_ENABLED, InsultImpl.class, insult.getPrimaryKey(), insult);
		
		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G, new Object[] { insult.getUuid(), insult.getGroupId() }, insult);
		
		insult.resetOriginalValues();
	}
	
	/**
	 * Caches the insults in the entity cache if it is enabled.
	 *
	 * @param insults
	 *            the insults
	 */
	@Override
	public void cacheResult(List<Insult> insults) {
		for (Insult insult : insults) {
			if (EntityCacheUtil.getResult(InsultModelImpl.ENTITY_CACHE_ENABLED, InsultImpl.class, insult.getPrimaryKey()) == null) {
				cacheResult(insult);
			} else {
				insult.resetOriginalValues();
			}
		}
	}
	
	/**
	 * Clears the cache for all insults.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and
	 * {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by
	 * this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(InsultImpl.class.getName());
		}
		
		EntityCacheUtil.clearCache(InsultImpl.class.getName());
		
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}
	
	/**
	 * Clears the cache for the insult.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and
	 * {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by
	 * this method.
	 * </p>
	 */
	@Override
	public void clearCache(Insult insult) {
		EntityCacheUtil.removeResult(InsultModelImpl.ENTITY_CACHE_ENABLED, InsultImpl.class, insult.getPrimaryKey());
		
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		
		clearUniqueFindersCache(insult);
	}
	
	@Override
	public void clearCache(List<Insult> insults) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		
		for (Insult insult : insults) {
			EntityCacheUtil.removeResult(InsultModelImpl.ENTITY_CACHE_ENABLED, InsultImpl.class, insult.getPrimaryKey());
			
			clearUniqueFindersCache(insult);
		}
	}
	
	protected void cacheUniqueFindersCache(Insult insult) {
		if (insult.isNew()) {
			Object[] args = new Object[] { insult.getUuid(), insult.getGroupId() };
			
			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_UUID_G, args, Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G, args, insult);
		} else {
			InsultModelImpl insultModelImpl = (InsultModelImpl) insult;
			
			if ((insultModelImpl.getColumnBitmask() & FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { insult.getUuid(), insult.getGroupId() };
				
				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_UUID_G, args, Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G, args, insult);
			}
		}
	}
	
	protected void clearUniqueFindersCache(Insult insult) {
		InsultModelImpl insultModelImpl = (InsultModelImpl) insult;
		
		Object[] args = new Object[] { insult.getUuid(), insult.getGroupId() };
		
		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);
		
		if ((insultModelImpl.getColumnBitmask() & FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
			args = new Object[] { insultModelImpl.getOriginalUuid(), insultModelImpl.getOriginalGroupId() };
			
			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);
		}
	}
	
	/**
	 * Creates a new insult with the primary key. Does not add the insult to the
	 * database.
	 *
	 * @param insultId
	 *            the primary key for the new insult
	 * @return the new insult
	 */
	@Override
	public Insult create(long insultId) {
		Insult insult = new InsultImpl();
		
		insult.setNew(true);
		insult.setPrimaryKey(insultId);
		
		String uuid = PortalUUIDUtil.generate();
		
		insult.setUuid(uuid);
		
		return insult;
	}
	
	/**
	 * Removes the insult with the primary key from the database. Also notifies
	 * the appropriate model listeners.
	 *
	 * @param insultId
	 *            the primary key of the insult
	 * @return the insult that was removed
	 * @throws com.liferay.docs.insult.NoSuchInsultException
	 *             if a insult with the primary key could not be found
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public Insult remove(long insultId) throws NoSuchInsultException, SystemException {
		return remove((Serializable) insultId);
	}
	
	/**
	 * Removes the insult with the primary key from the database. Also notifies
	 * the appropriate model listeners.
	 *
	 * @param primaryKey
	 *            the primary key of the insult
	 * @return the insult that was removed
	 * @throws com.liferay.docs.insult.NoSuchInsultException
	 *             if a insult with the primary key could not be found
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public Insult remove(Serializable primaryKey) throws NoSuchInsultException, SystemException {
		Session session = null;
		
		try {
			session = openSession();
			
			Insult insult = (Insult) session.get(InsultImpl.class, primaryKey);
			
			if (insult == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}
				
				throw new NoSuchInsultException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}
			
			return remove(insult);
		} catch (NoSuchInsultException nsee) {
			throw nsee;
		} catch (Exception e) {
			throw processException(e);
		} finally {
			closeSession(session);
		}
	}
	
	@Override
	protected Insult removeImpl(Insult insult) throws SystemException {
		insult = toUnwrappedModel(insult);
		
		Session session = null;
		
		try {
			session = openSession();
			
			if (!session.contains(insult)) {
				insult = (Insult) session.get(InsultImpl.class, insult.getPrimaryKeyObj());
			}
			
			if (insult != null) {
				session.delete(insult);
			}
		} catch (Exception e) {
			throw processException(e);
		} finally {
			closeSession(session);
		}
		
		if (insult != null) {
			clearCache(insult);
		}
		
		return insult;
	}
	
	@Override
	public Insult updateImpl(com.liferay.docs.insult.model.Insult insult) throws SystemException {
		insult = toUnwrappedModel(insult);
		
		boolean isNew = insult.isNew();
		
		InsultModelImpl insultModelImpl = (InsultModelImpl) insult;
		
		if (Validator.isNull(insult.getUuid())) {
			String uuid = PortalUUIDUtil.generate();
			
			insult.setUuid(uuid);
		}
		
		Session session = null;
		
		try {
			session = openSession();
			
			if (insult.isNew()) {
				session.save(insult);
				
				insult.setNew(false);
			} else {
				session.merge(insult);
			}
		} catch (Exception e) {
			throw processException(e);
		} finally {
			closeSession(session);
		}
		
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		
		if (isNew || !InsultModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		
		else {
			if ((insultModelImpl.getColumnBitmask() & FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { insultModelImpl.getOriginalUuid() };
				
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID, args);
				
				args = new Object[] { insultModelImpl.getUuid() };
				
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID, args);
			}
			
			if ((insultModelImpl.getColumnBitmask() & FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { insultModelImpl.getOriginalUuid(), insultModelImpl.getOriginalCompanyId() };
				
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C, args);
				
				args = new Object[] { insultModelImpl.getUuid(), insultModelImpl.getCompanyId() };
				
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C, args);
			}
			
			if ((insultModelImpl.getColumnBitmask() & FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { insultModelImpl.getOriginalGroupId() };
				
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_GROUPID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID, args);
				
				args = new Object[] { insultModelImpl.getGroupId() };
				
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_GROUPID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID, args);
			}
		}
		
		EntityCacheUtil.putResult(InsultModelImpl.ENTITY_CACHE_ENABLED, InsultImpl.class, insult.getPrimaryKey(), insult);
		
		clearUniqueFindersCache(insult);
		cacheUniqueFindersCache(insult);
		
		return insult;
	}
	
	protected Insult toUnwrappedModel(Insult insult) {
		if (insult instanceof InsultImpl) {
			return insult;
		}
		
		InsultImpl insultImpl = new InsultImpl();
		
		insultImpl.setNew(insult.isNew());
		insultImpl.setPrimaryKey(insult.getPrimaryKey());
		
		insultImpl.setUuid(insult.getUuid());
		insultImpl.setInsultId(insult.getInsultId());
		insultImpl.setInsultString(insult.getInsultString());
		insultImpl.setUserId(insult.getUserId());
		insultImpl.setGroupId(insult.getGroupId());
		insultImpl.setCompanyId(insult.getCompanyId());
		insultImpl.setUserName(insult.getUserName());
		insultImpl.setCreateDate(insult.getCreateDate());
		insultImpl.setModifiedDate(insult.getModifiedDate());
		
		return insultImpl;
	}
	
	/**
	 * Returns the insult with the primary key or throws a
	 * {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey
	 *            the primary key of the insult
	 * @return the insult
	 * @throws com.liferay.docs.insult.NoSuchInsultException
	 *             if a insult with the primary key could not be found
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public Insult findByPrimaryKey(Serializable primaryKey) throws NoSuchInsultException, SystemException {
		Insult insult = fetchByPrimaryKey(primaryKey);
		
		if (insult == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}
			
			throw new NoSuchInsultException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}
		
		return insult;
	}
	
	/**
	 * Returns the insult with the primary key or throws a
	 * {@link com.liferay.docs.insult.NoSuchInsultException} if it could not be
	 * found.
	 *
	 * @param insultId
	 *            the primary key of the insult
	 * @return the insult
	 * @throws com.liferay.docs.insult.NoSuchInsultException
	 *             if a insult with the primary key could not be found
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public Insult findByPrimaryKey(long insultId) throws NoSuchInsultException, SystemException {
		return findByPrimaryKey((Serializable) insultId);
	}
	
	/**
	 * Returns the insult with the primary key or returns <code>null</code> if
	 * it could not be found.
	 *
	 * @param primaryKey
	 *            the primary key of the insult
	 * @return the insult, or <code>null</code> if a insult with the primary key
	 *         could not be found
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public Insult fetchByPrimaryKey(Serializable primaryKey) throws SystemException {
		Insult insult = (Insult) EntityCacheUtil.getResult(InsultModelImpl.ENTITY_CACHE_ENABLED, InsultImpl.class, primaryKey);
		
		if (insult == _nullInsult) {
			return null;
		}
		
		if (insult == null) {
			Session session = null;
			
			try {
				session = openSession();
				
				insult = (Insult) session.get(InsultImpl.class, primaryKey);
				
				if (insult != null) {
					cacheResult(insult);
				} else {
					EntityCacheUtil.putResult(InsultModelImpl.ENTITY_CACHE_ENABLED, InsultImpl.class, primaryKey, _nullInsult);
				}
			} catch (Exception e) {
				EntityCacheUtil.removeResult(InsultModelImpl.ENTITY_CACHE_ENABLED, InsultImpl.class, primaryKey);
				
				throw processException(e);
			} finally {
				closeSession(session);
			}
		}
		
		return insult;
	}
	
	/**
	 * Returns the insult with the primary key or returns <code>null</code> if
	 * it could not be found.
	 *
	 * @param insultId
	 *            the primary key of the insult
	 * @return the insult, or <code>null</code> if a insult with the primary key
	 *         could not be found
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public Insult fetchByPrimaryKey(long insultId) throws SystemException {
		return fetchByPrimaryKey((Serializable) insultId);
	}
	
	/**
	 * Returns all the insults.
	 *
	 * @return the insults
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public List<Insult> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}
	
	/**
	 * Returns a range of all the insults.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of
	 * <code>end - start</code> instances. <code>start</code> and
	 * <code>end</code> are not primary keys, they are indexes in the result
	 * set. Thus, <code>0</code> refers to the first result in the set. Setting
	 * both <code>start</code> and <code>end</code> to
	 * {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return
	 * the full result set. If <code>orderByComparator</code> is specified, then
	 * the query will include the given ORDER BY logic. If
	 * <code>orderByComparator</code> is absent and pagination is required (
	 * <code>start</code> and <code>end</code> are not
	 * {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the
	 * query will include the default ORDER BY logic from
	 * {@link com.liferay.docs.insult.model.impl.InsultModelImpl}. If both
	 * <code>orderByComparator</code> and pagination are absent, for performance
	 * reasons, the query will not have an ORDER BY clause and the returned
	 * result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start
	 *            the lower bound of the range of insults
	 * @param end
	 *            the upper bound of the range of insults (not inclusive)
	 * @return the range of insults
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public List<Insult> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}
	
	/**
	 * Returns an ordered range of all the insults.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of
	 * <code>end - start</code> instances. <code>start</code> and
	 * <code>end</code> are not primary keys, they are indexes in the result
	 * set. Thus, <code>0</code> refers to the first result in the set. Setting
	 * both <code>start</code> and <code>end</code> to
	 * {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return
	 * the full result set. If <code>orderByComparator</code> is specified, then
	 * the query will include the given ORDER BY logic. If
	 * <code>orderByComparator</code> is absent and pagination is required (
	 * <code>start</code> and <code>end</code> are not
	 * {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the
	 * query will include the default ORDER BY logic from
	 * {@link com.liferay.docs.insult.model.impl.InsultModelImpl}. If both
	 * <code>orderByComparator</code> and pagination are absent, for performance
	 * reasons, the query will not have an ORDER BY clause and the returned
	 * result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start
	 *            the lower bound of the range of insults
	 * @param end
	 *            the upper bound of the range of insults (not inclusive)
	 * @param orderByComparator
	 *            the comparator to order the results by (optionally
	 *            <code>null</code>)
	 * @return the ordered range of insults
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public List<Insult> findAll(int start, int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;
		
		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) && (orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		} else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}
		
		List<Insult> list = (List<Insult>) FinderCacheUtil.getResult(finderPath, finderArgs, this);
		
		if (list == null) {
			StringBundler query = null;
			String sql = null;
			
			if (orderByComparator != null) {
				query = new StringBundler(2 + (orderByComparator.getOrderByFields().length * 3));
				
				query.append(_SQL_SELECT_INSULT);
				
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
				
				sql = query.toString();
			} else {
				sql = _SQL_SELECT_INSULT;
				
				if (pagination) {
					sql = sql.concat(InsultModelImpl.ORDER_BY_JPQL);
				}
			}
			
			Session session = null;
			
			try {
				session = openSession();
				
				Query q = session.createQuery(sql);
				
				if (!pagination) {
					list = (List<Insult>) QueryUtil.list(q, getDialect(), start, end, false);
					
					Collections.sort(list);
					
					list = new UnmodifiableList<Insult>(list);
				} else {
					list = (List<Insult>) QueryUtil.list(q, getDialect(), start, end);
				}
				
				cacheResult(list);
				
				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			} catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);
				
				throw processException(e);
			} finally {
				closeSession(session);
			}
		}
		
		return list;
	}
	
	/**
	 * Removes all the insults from the database.
	 *
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (Insult insult : findAll()) {
			remove(insult);
		}
	}
	
	/**
	 * Returns the number of insults.
	 *
	 * @return the number of insults
	 * @throws SystemException
	 *             if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY, this);
		
		if (count == null) {
			Session session = null;
			
			try {
				session = openSession();
				
				Query q = session.createQuery(_SQL_COUNT_INSULT);
				
				count = (Long) q.uniqueResult();
				
				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY, count);
			} catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
				
				throw processException(e);
			} finally {
				closeSession(session);
			}
		}
		
		return count.intValue();
	}
	
	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}
	
	/**
	 * Initializes the insult persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(com.liferay.util.service.ServiceProps
				.get("value.object.listener.com.liferay.docs.insult.model.Insult")));
		
		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Insult>> listenersList = new ArrayList<ModelListener<Insult>>();
				
				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Insult>) InstanceFactory.newInstance(getClassLoader(), listenerClassName));
				}
				
				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			} catch (Exception e) {
				_log.error(e);
			}
		}
	}
	
	public void destroy() {
		EntityCacheUtil.removeCache(InsultImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}
	
	private static final String _SQL_SELECT_INSULT = "SELECT insult FROM Insult insult";
	private static final String _SQL_SELECT_INSULT_WHERE = "SELECT insult FROM Insult insult WHERE ";
	private static final String _SQL_COUNT_INSULT = "SELECT COUNT(insult) FROM Insult insult";
	private static final String _SQL_COUNT_INSULT_WHERE = "SELECT COUNT(insult) FROM Insult insult WHERE ";
	private static final String _FILTER_ENTITY_TABLE_FILTER_PK_COLUMN = "insult.insultId";
	private static final String _FILTER_SQL_SELECT_INSULT_WHERE = "SELECT DISTINCT {insult.*} FROM Insult_Insult insult WHERE ";
	private static final String _FILTER_SQL_SELECT_INSULT_NO_INLINE_DISTINCT_WHERE_1 = "SELECT {Insult_Insult.*} FROM (SELECT DISTINCT insult.insultId FROM Insult_Insult insult WHERE ";
	private static final String _FILTER_SQL_SELECT_INSULT_NO_INLINE_DISTINCT_WHERE_2 = ") TEMP_TABLE INNER JOIN Insult_Insult ON TEMP_TABLE.insultId = Insult_Insult.insultId";
	private static final String _FILTER_SQL_COUNT_INSULT_WHERE = "SELECT COUNT(DISTINCT insult.insultId) AS COUNT_VALUE FROM Insult_Insult insult WHERE ";
	private static final String _FILTER_ENTITY_ALIAS = "insult";
	private static final String _FILTER_ENTITY_TABLE = "Insult_Insult";
	private static final String _ORDER_BY_ENTITY_ALIAS = "insult.";
	private static final String _ORDER_BY_ENTITY_TABLE = "Insult_Insult.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Insult exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Insult exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil
			.get(PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(InsultPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] { "uuid" });
	private static Insult _nullInsult = new InsultImpl() {
		@Override
		public Object clone() {
			return this;
		}
		
		@Override
		public CacheModel<Insult> toCacheModel() {
			return _nullInsultCacheModel;
		}
	};
	
	private static CacheModel<Insult> _nullInsultCacheModel = new CacheModel<Insult>() {
		@Override
		public Insult toEntityModel() {
			return _nullInsult;
		}
	};
}