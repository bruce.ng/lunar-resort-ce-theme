/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package dtt.nuocsach.dao.sync.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc;

import java.util.List;

/**
 * The persistence utility for the thong tin kiem dem nuoc service. This utility wraps {@link ThongTinKiemDemNuocPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author quangtrung
 * @see ThongTinKiemDemNuocPersistence
 * @see ThongTinKiemDemNuocPersistenceImpl
 * @generated
 */
public class ThongTinKiemDemNuocUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(ThongTinKiemDemNuoc thongTinKiemDemNuoc) {
		getPersistence().clearCache(thongTinKiemDemNuoc);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ThongTinKiemDemNuoc> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ThongTinKiemDemNuoc> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ThongTinKiemDemNuoc> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static ThongTinKiemDemNuoc update(
		ThongTinKiemDemNuoc thongTinKiemDemNuoc) throws SystemException {
		return getPersistence().update(thongTinKiemDemNuoc);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static ThongTinKiemDemNuoc update(
		ThongTinKiemDemNuoc thongTinKiemDemNuoc, ServiceContext serviceContext)
		throws SystemException {
		return getPersistence().update(thongTinKiemDemNuoc, serviceContext);
	}

	/**
	* Returns all the thong tin kiem dem nuocs where dauNoiNuocId = &#63;.
	*
	* @param dauNoiNuocId the dau noi nuoc ID
	* @return the matching thong tin kiem dem nuocs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc> findByD_dauNoiNuocId(
		long dauNoiNuocId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByD_dauNoiNuocId(dauNoiNuocId);
	}

	/**
	* Returns a range of all the thong tin kiem dem nuocs where dauNoiNuocId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link dtt.nuocsach.dao.sync.model.impl.ThongTinKiemDemNuocModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dauNoiNuocId the dau noi nuoc ID
	* @param start the lower bound of the range of thong tin kiem dem nuocs
	* @param end the upper bound of the range of thong tin kiem dem nuocs (not inclusive)
	* @return the range of matching thong tin kiem dem nuocs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc> findByD_dauNoiNuocId(
		long dauNoiNuocId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByD_dauNoiNuocId(dauNoiNuocId, start, end);
	}

	/**
	* Returns an ordered range of all the thong tin kiem dem nuocs where dauNoiNuocId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link dtt.nuocsach.dao.sync.model.impl.ThongTinKiemDemNuocModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dauNoiNuocId the dau noi nuoc ID
	* @param start the lower bound of the range of thong tin kiem dem nuocs
	* @param end the upper bound of the range of thong tin kiem dem nuocs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching thong tin kiem dem nuocs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc> findByD_dauNoiNuocId(
		long dauNoiNuocId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByD_dauNoiNuocId(dauNoiNuocId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first thong tin kiem dem nuoc in the ordered set where dauNoiNuocId = &#63;.
	*
	* @param dauNoiNuocId the dau noi nuoc ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching thong tin kiem dem nuoc
	* @throws dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException if a matching thong tin kiem dem nuoc could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc findByD_dauNoiNuocId_First(
		long dauNoiNuocId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException {
		return getPersistence()
				   .findByD_dauNoiNuocId_First(dauNoiNuocId, orderByComparator);
	}

	/**
	* Returns the first thong tin kiem dem nuoc in the ordered set where dauNoiNuocId = &#63;.
	*
	* @param dauNoiNuocId the dau noi nuoc ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching thong tin kiem dem nuoc, or <code>null</code> if a matching thong tin kiem dem nuoc could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc fetchByD_dauNoiNuocId_First(
		long dauNoiNuocId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByD_dauNoiNuocId_First(dauNoiNuocId, orderByComparator);
	}

	/**
	* Returns the last thong tin kiem dem nuoc in the ordered set where dauNoiNuocId = &#63;.
	*
	* @param dauNoiNuocId the dau noi nuoc ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching thong tin kiem dem nuoc
	* @throws dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException if a matching thong tin kiem dem nuoc could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc findByD_dauNoiNuocId_Last(
		long dauNoiNuocId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException {
		return getPersistence()
				   .findByD_dauNoiNuocId_Last(dauNoiNuocId, orderByComparator);
	}

	/**
	* Returns the last thong tin kiem dem nuoc in the ordered set where dauNoiNuocId = &#63;.
	*
	* @param dauNoiNuocId the dau noi nuoc ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching thong tin kiem dem nuoc, or <code>null</code> if a matching thong tin kiem dem nuoc could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc fetchByD_dauNoiNuocId_Last(
		long dauNoiNuocId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByD_dauNoiNuocId_Last(dauNoiNuocId, orderByComparator);
	}

	/**
	* Returns the thong tin kiem dem nuocs before and after the current thong tin kiem dem nuoc in the ordered set where dauNoiNuocId = &#63;.
	*
	* @param id the primary key of the current thong tin kiem dem nuoc
	* @param dauNoiNuocId the dau noi nuoc ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next thong tin kiem dem nuoc
	* @throws dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException if a thong tin kiem dem nuoc with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc[] findByD_dauNoiNuocId_PrevAndNext(
		long id, long dauNoiNuocId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException {
		return getPersistence()
				   .findByD_dauNoiNuocId_PrevAndNext(id, dauNoiNuocId,
			orderByComparator);
	}

	/**
	* Removes all the thong tin kiem dem nuocs where dauNoiNuocId = &#63; from the database.
	*
	* @param dauNoiNuocId the dau noi nuoc ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByD_dauNoiNuocId(long dauNoiNuocId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByD_dauNoiNuocId(dauNoiNuocId);
	}

	/**
	* Returns the number of thong tin kiem dem nuocs where dauNoiNuocId = &#63;.
	*
	* @param dauNoiNuocId the dau noi nuoc ID
	* @return the number of matching thong tin kiem dem nuocs
	* @throws SystemException if a system exception occurred
	*/
	public static int countByD_dauNoiNuocId(long dauNoiNuocId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByD_dauNoiNuocId(dauNoiNuocId);
	}

	/**
	* Caches the thong tin kiem dem nuoc in the entity cache if it is enabled.
	*
	* @param thongTinKiemDemNuoc the thong tin kiem dem nuoc
	*/
	public static void cacheResult(
		dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc thongTinKiemDemNuoc) {
		getPersistence().cacheResult(thongTinKiemDemNuoc);
	}

	/**
	* Caches the thong tin kiem dem nuocs in the entity cache if it is enabled.
	*
	* @param thongTinKiemDemNuocs the thong tin kiem dem nuocs
	*/
	public static void cacheResult(
		java.util.List<dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc> thongTinKiemDemNuocs) {
		getPersistence().cacheResult(thongTinKiemDemNuocs);
	}

	/**
	* Creates a new thong tin kiem dem nuoc with the primary key. Does not add the thong tin kiem dem nuoc to the database.
	*
	* @param id the primary key for the new thong tin kiem dem nuoc
	* @return the new thong tin kiem dem nuoc
	*/
	public static dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc create(
		long id) {
		return getPersistence().create(id);
	}

	/**
	* Removes the thong tin kiem dem nuoc with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the thong tin kiem dem nuoc
	* @return the thong tin kiem dem nuoc that was removed
	* @throws dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException if a thong tin kiem dem nuoc with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc remove(
		long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException {
		return getPersistence().remove(id);
	}

	public static dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc updateImpl(
		dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc thongTinKiemDemNuoc)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(thongTinKiemDemNuoc);
	}

	/**
	* Returns the thong tin kiem dem nuoc with the primary key or throws a {@link dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException} if it could not be found.
	*
	* @param id the primary key of the thong tin kiem dem nuoc
	* @return the thong tin kiem dem nuoc
	* @throws dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException if a thong tin kiem dem nuoc with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc findByPrimaryKey(
		long id)
		throws com.liferay.portal.kernel.exception.SystemException,
			dtt.nuocsach.dao.sync.NoSuchThongTinKiemDemNuocException {
		return getPersistence().findByPrimaryKey(id);
	}

	/**
	* Returns the thong tin kiem dem nuoc with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param id the primary key of the thong tin kiem dem nuoc
	* @return the thong tin kiem dem nuoc, or <code>null</code> if a thong tin kiem dem nuoc with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc fetchByPrimaryKey(
		long id) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(id);
	}

	/**
	* Returns all the thong tin kiem dem nuocs.
	*
	* @return the thong tin kiem dem nuocs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the thong tin kiem dem nuocs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link dtt.nuocsach.dao.sync.model.impl.ThongTinKiemDemNuocModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of thong tin kiem dem nuocs
	* @param end the upper bound of the range of thong tin kiem dem nuocs (not inclusive)
	* @return the range of thong tin kiem dem nuocs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the thong tin kiem dem nuocs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link dtt.nuocsach.dao.sync.model.impl.ThongTinKiemDemNuocModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of thong tin kiem dem nuocs
	* @param end the upper bound of the range of thong tin kiem dem nuocs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of thong tin kiem dem nuocs
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<dtt.nuocsach.dao.sync.model.ThongTinKiemDemNuoc> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the thong tin kiem dem nuocs from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of thong tin kiem dem nuocs.
	*
	* @return the number of thong tin kiem dem nuocs
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static ThongTinKiemDemNuocPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (ThongTinKiemDemNuocPersistence)PortletBeanLocatorUtil.locate(dtt.nuocsach.dao.sync.service.ClpSerializer.getServletContextName(),
					ThongTinKiemDemNuocPersistence.class.getName());

			ReferenceRegistry.registerReference(ThongTinKiemDemNuocUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(ThongTinKiemDemNuocPersistence persistence) {
	}

	private static ThongTinKiemDemNuocPersistence _persistence;
}