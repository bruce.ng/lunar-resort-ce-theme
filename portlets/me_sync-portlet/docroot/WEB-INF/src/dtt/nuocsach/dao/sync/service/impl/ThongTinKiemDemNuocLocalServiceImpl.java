/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package dtt.nuocsach.dao.sync.service.impl;

import dtt.nuocsach.dao.sync.service.base.ThongTinKiemDemNuocLocalServiceBaseImpl;

/**
 * The implementation of the thong tin kiem dem nuoc local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link dtt.nuocsach.dao.sync.service.ThongTinKiemDemNuocLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author quangtrung
 * @see dtt.nuocsach.dao.sync.service.base.ThongTinKiemDemNuocLocalServiceBaseImpl
 * @see dtt.nuocsach.dao.sync.service.ThongTinKiemDemNuocLocalServiceUtil
 */
public class ThongTinKiemDemNuocLocalServiceImpl
	extends ThongTinKiemDemNuocLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link dtt.nuocsach.dao.sync.service.ThongTinKiemDemNuocLocalServiceUtil} to access the thong tin kiem dem nuoc local service.
	 */
}