package vn.dtt.sol.cmon.user.business;

import java.util.ArrayList;
import java.util.List;

import vn.dtt.sol.cmon.user.dao.model.UserMapping;
import vn.dtt.sol.cmon.user.dao.service.UserMappingLocalServiceUtil;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class UserMappingBusiness {

	/**
	 * @param orgCode1
	 * @return
	 */
	public static List<UserMapping> getByOrgCode1(String orgCode1) {

		List<UserMapping> list = new ArrayList<UserMapping>();

		try {
			list = UserMappingLocalServiceUtil.getByOrg1(orgCode1);
		} catch (Exception e) {
			_log.error(e);
		}

		return list;
	}

	/**
	 * @param orgCode2
	 * @return
	 */
	public static List<UserMapping> getByOrgCode2(String orgCode2) {

		List<UserMapping> list = new ArrayList<UserMapping>();

		try {
			list = UserMappingLocalServiceUtil.getByOrg2(orgCode2);
		} catch (Exception e) {
			_log.error(e);
		}

		return list;
	}

	/**
	 * @param orgCode1
	 * @param orgCode2
	 * @return
	 */
	public static List<UserMapping> getByOrgCode1_OrgCode2(String orgCode1,
			String orgCode2) {

		List<UserMapping> list = new ArrayList<UserMapping>();

		try {
			list = UserMappingLocalServiceUtil.getByOrg1_Org2(orgCode1,
					orgCode2);
		} catch (Exception e) {
			_log.error(e);
		}

		return list;
	}
	
	/**
	 * @param userType
	 * @param orgCode1
	 * @return
	 */
	public static List<UserMapping> getByTypeOrg1(int userType, String orgCode1) {

		List<UserMapping> list = new ArrayList<UserMapping>();

		try {
			list = UserMappingLocalServiceUtil.getByTypeOrg1(userType, orgCode1);
		} catch (Exception e) {
			_log.error(e);
		}

		return list;
	}

	public static List<UserMapping> getByTypeOrg2(int userType,String orgCode1, String orgCode2) {

		List<UserMapping> list = new ArrayList<UserMapping>();

		try {
			list = UserMappingLocalServiceUtil.getByTypeOrg2(userType, orgCode1, orgCode2);
		} catch (Exception e) {
			_log.error(e);
		}

		return list;
	}

	private static Log _log = LogFactoryUtil.getLog(UserMappingBusiness.class);
}
