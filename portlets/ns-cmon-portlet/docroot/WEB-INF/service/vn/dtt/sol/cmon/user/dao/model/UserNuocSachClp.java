/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package vn.dtt.sol.cmon.user.dao.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import vn.dtt.sol.cmon.user.dao.service.ClpSerializer;
import vn.dtt.sol.cmon.user.dao.service.UserNuocSachLocalServiceUtil;
import vn.dtt.sol.cmon.user.dao.service.persistence.UserNuocSachPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author khoa.vu
 */
public class UserNuocSachClp extends BaseModelImpl<UserNuocSach>
	implements UserNuocSach {
	public UserNuocSachClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return UserNuocSach.class;
	}

	@Override
	public String getModelClassName() {
		return UserNuocSach.class.getName();
	}

	@Override
	public UserNuocSachPK getPrimaryKey() {
		return new UserNuocSachPK(_id, _mapUserId);
	}

	@Override
	public void setPrimaryKey(UserNuocSachPK primaryKey) {
		setId(primaryKey.id);
		setMapUserId(primaryKey.mapUserId);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new UserNuocSachPK(_id, _mapUserId);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((UserNuocSachPK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("id", getId());
		attributes.put("mapUserId", getMapUserId());
		attributes.put("ten", getTen());
		attributes.put("trangThai", getTrangThai());
		attributes.put("ngayTao", getNgayTao());
		attributes.put("ngaySua", getNgaySua());
		attributes.put("nguoiTao", getNguoiTao());
		attributes.put("nguoiSua", getNguoiSua());
		attributes.put("toChucId", getToChucId());
		attributes.put("screenName", getScreenName());
		attributes.put("emailAddress", getEmailAddress());
		attributes.put("ngaySinh", getNgaySinh());
		attributes.put("gioiTinh", getGioiTinh());
		attributes.put("matKhau", getMatKhau());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long id = (Long)attributes.get("id");

		if (id != null) {
			setId(id);
		}

		Long mapUserId = (Long)attributes.get("mapUserId");

		if (mapUserId != null) {
			setMapUserId(mapUserId);
		}

		String ten = (String)attributes.get("ten");

		if (ten != null) {
			setTen(ten);
		}

		Boolean trangThai = (Boolean)attributes.get("trangThai");

		if (trangThai != null) {
			setTrangThai(trangThai);
		}

		Date ngayTao = (Date)attributes.get("ngayTao");

		if (ngayTao != null) {
			setNgayTao(ngayTao);
		}

		Date ngaySua = (Date)attributes.get("ngaySua");

		if (ngaySua != null) {
			setNgaySua(ngaySua);
		}

		String nguoiTao = (String)attributes.get("nguoiTao");

		if (nguoiTao != null) {
			setNguoiTao(nguoiTao);
		}

		String nguoiSua = (String)attributes.get("nguoiSua");

		if (nguoiSua != null) {
			setNguoiSua(nguoiSua);
		}

		Long toChucId = (Long)attributes.get("toChucId");

		if (toChucId != null) {
			setToChucId(toChucId);
		}

		String screenName = (String)attributes.get("screenName");

		if (screenName != null) {
			setScreenName(screenName);
		}

		String emailAddress = (String)attributes.get("emailAddress");

		if (emailAddress != null) {
			setEmailAddress(emailAddress);
		}

		Date ngaySinh = (Date)attributes.get("ngaySinh");

		if (ngaySinh != null) {
			setNgaySinh(ngaySinh);
		}

		Boolean gioiTinh = (Boolean)attributes.get("gioiTinh");

		if (gioiTinh != null) {
			setGioiTinh(gioiTinh);
		}

		String matKhau = (String)attributes.get("matKhau");

		if (matKhau != null) {
			setMatKhau(matKhau);
		}
	}

	@Override
	public long getId() {
		return _id;
	}

	@Override
	public void setId(long id) {
		_id = id;

		if (_userNuocSachRemoteModel != null) {
			try {
				Class<?> clazz = _userNuocSachRemoteModel.getClass();

				Method method = clazz.getMethod("setId", long.class);

				method.invoke(_userNuocSachRemoteModel, id);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getMapUserId() {
		return _mapUserId;
	}

	@Override
	public void setMapUserId(long mapUserId) {
		_mapUserId = mapUserId;

		if (_userNuocSachRemoteModel != null) {
			try {
				Class<?> clazz = _userNuocSachRemoteModel.getClass();

				Method method = clazz.getMethod("setMapUserId", long.class);

				method.invoke(_userNuocSachRemoteModel, mapUserId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getMapUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getMapUserId(), "uuid", _mapUserUuid);
	}

	@Override
	public void setMapUserUuid(String mapUserUuid) {
		_mapUserUuid = mapUserUuid;
	}

	@Override
	public String getTen() {
		return _ten;
	}

	@Override
	public void setTen(String ten) {
		_ten = ten;

		if (_userNuocSachRemoteModel != null) {
			try {
				Class<?> clazz = _userNuocSachRemoteModel.getClass();

				Method method = clazz.getMethod("setTen", String.class);

				method.invoke(_userNuocSachRemoteModel, ten);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getTrangThai() {
		return _trangThai;
	}

	@Override
	public boolean isTrangThai() {
		return _trangThai;
	}

	@Override
	public void setTrangThai(boolean trangThai) {
		_trangThai = trangThai;

		if (_userNuocSachRemoteModel != null) {
			try {
				Class<?> clazz = _userNuocSachRemoteModel.getClass();

				Method method = clazz.getMethod("setTrangThai", boolean.class);

				method.invoke(_userNuocSachRemoteModel, trangThai);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getNgayTao() {
		return _ngayTao;
	}

	@Override
	public void setNgayTao(Date ngayTao) {
		_ngayTao = ngayTao;

		if (_userNuocSachRemoteModel != null) {
			try {
				Class<?> clazz = _userNuocSachRemoteModel.getClass();

				Method method = clazz.getMethod("setNgayTao", Date.class);

				method.invoke(_userNuocSachRemoteModel, ngayTao);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getNgaySua() {
		return _ngaySua;
	}

	@Override
	public void setNgaySua(Date ngaySua) {
		_ngaySua = ngaySua;

		if (_userNuocSachRemoteModel != null) {
			try {
				Class<?> clazz = _userNuocSachRemoteModel.getClass();

				Method method = clazz.getMethod("setNgaySua", Date.class);

				method.invoke(_userNuocSachRemoteModel, ngaySua);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNguoiTao() {
		return _nguoiTao;
	}

	@Override
	public void setNguoiTao(String nguoiTao) {
		_nguoiTao = nguoiTao;

		if (_userNuocSachRemoteModel != null) {
			try {
				Class<?> clazz = _userNuocSachRemoteModel.getClass();

				Method method = clazz.getMethod("setNguoiTao", String.class);

				method.invoke(_userNuocSachRemoteModel, nguoiTao);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNguoiSua() {
		return _nguoiSua;
	}

	@Override
	public void setNguoiSua(String nguoiSua) {
		_nguoiSua = nguoiSua;

		if (_userNuocSachRemoteModel != null) {
			try {
				Class<?> clazz = _userNuocSachRemoteModel.getClass();

				Method method = clazz.getMethod("setNguoiSua", String.class);

				method.invoke(_userNuocSachRemoteModel, nguoiSua);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getToChucId() {
		return _toChucId;
	}

	@Override
	public void setToChucId(long toChucId) {
		_toChucId = toChucId;

		if (_userNuocSachRemoteModel != null) {
			try {
				Class<?> clazz = _userNuocSachRemoteModel.getClass();

				Method method = clazz.getMethod("setToChucId", long.class);

				method.invoke(_userNuocSachRemoteModel, toChucId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getScreenName() {
		return _screenName;
	}

	@Override
	public void setScreenName(String screenName) {
		_screenName = screenName;

		if (_userNuocSachRemoteModel != null) {
			try {
				Class<?> clazz = _userNuocSachRemoteModel.getClass();

				Method method = clazz.getMethod("setScreenName", String.class);

				method.invoke(_userNuocSachRemoteModel, screenName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getEmailAddress() {
		return _emailAddress;
	}

	@Override
	public void setEmailAddress(String emailAddress) {
		_emailAddress = emailAddress;

		if (_userNuocSachRemoteModel != null) {
			try {
				Class<?> clazz = _userNuocSachRemoteModel.getClass();

				Method method = clazz.getMethod("setEmailAddress", String.class);

				method.invoke(_userNuocSachRemoteModel, emailAddress);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getNgaySinh() {
		return _ngaySinh;
	}

	@Override
	public void setNgaySinh(Date ngaySinh) {
		_ngaySinh = ngaySinh;

		if (_userNuocSachRemoteModel != null) {
			try {
				Class<?> clazz = _userNuocSachRemoteModel.getClass();

				Method method = clazz.getMethod("setNgaySinh", Date.class);

				method.invoke(_userNuocSachRemoteModel, ngaySinh);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getGioiTinh() {
		return _gioiTinh;
	}

	@Override
	public boolean isGioiTinh() {
		return _gioiTinh;
	}

	@Override
	public void setGioiTinh(boolean gioiTinh) {
		_gioiTinh = gioiTinh;

		if (_userNuocSachRemoteModel != null) {
			try {
				Class<?> clazz = _userNuocSachRemoteModel.getClass();

				Method method = clazz.getMethod("setGioiTinh", boolean.class);

				method.invoke(_userNuocSachRemoteModel, gioiTinh);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getMatKhau() {
		return _matKhau;
	}

	@Override
	public void setMatKhau(String matKhau) {
		_matKhau = matKhau;

		if (_userNuocSachRemoteModel != null) {
			try {
				Class<?> clazz = _userNuocSachRemoteModel.getClass();

				Method method = clazz.getMethod("setMatKhau", String.class);

				method.invoke(_userNuocSachRemoteModel, matKhau);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getUserNuocSachRemoteModel() {
		return _userNuocSachRemoteModel;
	}

	public void setUserNuocSachRemoteModel(BaseModel<?> userNuocSachRemoteModel) {
		_userNuocSachRemoteModel = userNuocSachRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _userNuocSachRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_userNuocSachRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			UserNuocSachLocalServiceUtil.addUserNuocSach(this);
		}
		else {
			UserNuocSachLocalServiceUtil.updateUserNuocSach(this);
		}
	}

	@Override
	public UserNuocSach toEscapedModel() {
		return (UserNuocSach)ProxyUtil.newProxyInstance(UserNuocSach.class.getClassLoader(),
			new Class[] { UserNuocSach.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		UserNuocSachClp clone = new UserNuocSachClp();

		clone.setId(getId());
		clone.setMapUserId(getMapUserId());
		clone.setTen(getTen());
		clone.setTrangThai(getTrangThai());
		clone.setNgayTao(getNgayTao());
		clone.setNgaySua(getNgaySua());
		clone.setNguoiTao(getNguoiTao());
		clone.setNguoiSua(getNguoiSua());
		clone.setToChucId(getToChucId());
		clone.setScreenName(getScreenName());
		clone.setEmailAddress(getEmailAddress());
		clone.setNgaySinh(getNgaySinh());
		clone.setGioiTinh(getGioiTinh());
		clone.setMatKhau(getMatKhau());

		return clone;
	}

	@Override
	public int compareTo(UserNuocSach userNuocSach) {
		int value = 0;

		value = getTen().compareTo(userNuocSach.getTen());

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserNuocSachClp)) {
			return false;
		}

		UserNuocSachClp userNuocSach = (UserNuocSachClp)obj;

		UserNuocSachPK primaryKey = userNuocSach.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(29);

		sb.append("{id=");
		sb.append(getId());
		sb.append(", mapUserId=");
		sb.append(getMapUserId());
		sb.append(", ten=");
		sb.append(getTen());
		sb.append(", trangThai=");
		sb.append(getTrangThai());
		sb.append(", ngayTao=");
		sb.append(getNgayTao());
		sb.append(", ngaySua=");
		sb.append(getNgaySua());
		sb.append(", nguoiTao=");
		sb.append(getNguoiTao());
		sb.append(", nguoiSua=");
		sb.append(getNguoiSua());
		sb.append(", toChucId=");
		sb.append(getToChucId());
		sb.append(", screenName=");
		sb.append(getScreenName());
		sb.append(", emailAddress=");
		sb.append(getEmailAddress());
		sb.append(", ngaySinh=");
		sb.append(getNgaySinh());
		sb.append(", gioiTinh=");
		sb.append(getGioiTinh());
		sb.append(", matKhau=");
		sb.append(getMatKhau());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(46);

		sb.append("<model><model-name>");
		sb.append("vn.dtt.sol.cmon.user.dao.model.UserNuocSach");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>id</column-name><column-value><![CDATA[");
		sb.append(getId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>mapUserId</column-name><column-value><![CDATA[");
		sb.append(getMapUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ten</column-name><column-value><![CDATA[");
		sb.append(getTen());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>trangThai</column-name><column-value><![CDATA[");
		sb.append(getTrangThai());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ngayTao</column-name><column-value><![CDATA[");
		sb.append(getNgayTao());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ngaySua</column-name><column-value><![CDATA[");
		sb.append(getNgaySua());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>nguoiTao</column-name><column-value><![CDATA[");
		sb.append(getNguoiTao());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>nguoiSua</column-name><column-value><![CDATA[");
		sb.append(getNguoiSua());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>toChucId</column-name><column-value><![CDATA[");
		sb.append(getToChucId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>screenName</column-name><column-value><![CDATA[");
		sb.append(getScreenName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>emailAddress</column-name><column-value><![CDATA[");
		sb.append(getEmailAddress());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ngaySinh</column-name><column-value><![CDATA[");
		sb.append(getNgaySinh());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>gioiTinh</column-name><column-value><![CDATA[");
		sb.append(getGioiTinh());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>matKhau</column-name><column-value><![CDATA[");
		sb.append(getMatKhau());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _id;
	private long _mapUserId;
	private String _mapUserUuid;
	private String _ten;
	private boolean _trangThai;
	private Date _ngayTao;
	private Date _ngaySua;
	private String _nguoiTao;
	private String _nguoiSua;
	private long _toChucId;
	private String _screenName;
	private String _emailAddress;
	private Date _ngaySinh;
	private boolean _gioiTinh;
	private String _matKhau;
	private BaseModel<?> _userNuocSachRemoteModel;
}