/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package vn.dtt.sol.cmon.user.dao.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link UserNuocSach}.
 * </p>
 *
 * @author khoa.vu
 * @see UserNuocSach
 * @generated
 */
public class UserNuocSachWrapper implements UserNuocSach,
	ModelWrapper<UserNuocSach> {
	public UserNuocSachWrapper(UserNuocSach userNuocSach) {
		_userNuocSach = userNuocSach;
	}

	@Override
	public Class<?> getModelClass() {
		return UserNuocSach.class;
	}

	@Override
	public String getModelClassName() {
		return UserNuocSach.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("id", getId());
		attributes.put("mapUserId", getMapUserId());
		attributes.put("ten", getTen());
		attributes.put("trangThai", getTrangThai());
		attributes.put("ngayTao", getNgayTao());
		attributes.put("ngaySua", getNgaySua());
		attributes.put("nguoiTao", getNguoiTao());
		attributes.put("nguoiSua", getNguoiSua());
		attributes.put("toChucId", getToChucId());
		attributes.put("screenName", getScreenName());
		attributes.put("emailAddress", getEmailAddress());
		attributes.put("ngaySinh", getNgaySinh());
		attributes.put("gioiTinh", getGioiTinh());
		attributes.put("matKhau", getMatKhau());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long id = (Long)attributes.get("id");

		if (id != null) {
			setId(id);
		}

		Long mapUserId = (Long)attributes.get("mapUserId");

		if (mapUserId != null) {
			setMapUserId(mapUserId);
		}

		String ten = (String)attributes.get("ten");

		if (ten != null) {
			setTen(ten);
		}

		Boolean trangThai = (Boolean)attributes.get("trangThai");

		if (trangThai != null) {
			setTrangThai(trangThai);
		}

		Date ngayTao = (Date)attributes.get("ngayTao");

		if (ngayTao != null) {
			setNgayTao(ngayTao);
		}

		Date ngaySua = (Date)attributes.get("ngaySua");

		if (ngaySua != null) {
			setNgaySua(ngaySua);
		}

		String nguoiTao = (String)attributes.get("nguoiTao");

		if (nguoiTao != null) {
			setNguoiTao(nguoiTao);
		}

		String nguoiSua = (String)attributes.get("nguoiSua");

		if (nguoiSua != null) {
			setNguoiSua(nguoiSua);
		}

		Long toChucId = (Long)attributes.get("toChucId");

		if (toChucId != null) {
			setToChucId(toChucId);
		}

		String screenName = (String)attributes.get("screenName");

		if (screenName != null) {
			setScreenName(screenName);
		}

		String emailAddress = (String)attributes.get("emailAddress");

		if (emailAddress != null) {
			setEmailAddress(emailAddress);
		}

		Date ngaySinh = (Date)attributes.get("ngaySinh");

		if (ngaySinh != null) {
			setNgaySinh(ngaySinh);
		}

		Boolean gioiTinh = (Boolean)attributes.get("gioiTinh");

		if (gioiTinh != null) {
			setGioiTinh(gioiTinh);
		}

		String matKhau = (String)attributes.get("matKhau");

		if (matKhau != null) {
			setMatKhau(matKhau);
		}
	}

	/**
	* Returns the primary key of this user nuoc sach.
	*
	* @return the primary key of this user nuoc sach
	*/
	@Override
	public vn.dtt.sol.cmon.user.dao.service.persistence.UserNuocSachPK getPrimaryKey() {
		return _userNuocSach.getPrimaryKey();
	}

	/**
	* Sets the primary key of this user nuoc sach.
	*
	* @param primaryKey the primary key of this user nuoc sach
	*/
	@Override
	public void setPrimaryKey(
		vn.dtt.sol.cmon.user.dao.service.persistence.UserNuocSachPK primaryKey) {
		_userNuocSach.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the ID of this user nuoc sach.
	*
	* @return the ID of this user nuoc sach
	*/
	@Override
	public long getId() {
		return _userNuocSach.getId();
	}

	/**
	* Sets the ID of this user nuoc sach.
	*
	* @param id the ID of this user nuoc sach
	*/
	@Override
	public void setId(long id) {
		_userNuocSach.setId(id);
	}

	/**
	* Returns the map user ID of this user nuoc sach.
	*
	* @return the map user ID of this user nuoc sach
	*/
	@Override
	public long getMapUserId() {
		return _userNuocSach.getMapUserId();
	}

	/**
	* Sets the map user ID of this user nuoc sach.
	*
	* @param mapUserId the map user ID of this user nuoc sach
	*/
	@Override
	public void setMapUserId(long mapUserId) {
		_userNuocSach.setMapUserId(mapUserId);
	}

	/**
	* Returns the map user uuid of this user nuoc sach.
	*
	* @return the map user uuid of this user nuoc sach
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getMapUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userNuocSach.getMapUserUuid();
	}

	/**
	* Sets the map user uuid of this user nuoc sach.
	*
	* @param mapUserUuid the map user uuid of this user nuoc sach
	*/
	@Override
	public void setMapUserUuid(java.lang.String mapUserUuid) {
		_userNuocSach.setMapUserUuid(mapUserUuid);
	}

	/**
	* Returns the ten of this user nuoc sach.
	*
	* @return the ten of this user nuoc sach
	*/
	@Override
	public java.lang.String getTen() {
		return _userNuocSach.getTen();
	}

	/**
	* Sets the ten of this user nuoc sach.
	*
	* @param ten the ten of this user nuoc sach
	*/
	@Override
	public void setTen(java.lang.String ten) {
		_userNuocSach.setTen(ten);
	}

	/**
	* Returns the trang thai of this user nuoc sach.
	*
	* @return the trang thai of this user nuoc sach
	*/
	@Override
	public boolean getTrangThai() {
		return _userNuocSach.getTrangThai();
	}

	/**
	* Returns <code>true</code> if this user nuoc sach is trang thai.
	*
	* @return <code>true</code> if this user nuoc sach is trang thai; <code>false</code> otherwise
	*/
	@Override
	public boolean isTrangThai() {
		return _userNuocSach.isTrangThai();
	}

	/**
	* Sets whether this user nuoc sach is trang thai.
	*
	* @param trangThai the trang thai of this user nuoc sach
	*/
	@Override
	public void setTrangThai(boolean trangThai) {
		_userNuocSach.setTrangThai(trangThai);
	}

	/**
	* Returns the ngay tao of this user nuoc sach.
	*
	* @return the ngay tao of this user nuoc sach
	*/
	@Override
	public java.util.Date getNgayTao() {
		return _userNuocSach.getNgayTao();
	}

	/**
	* Sets the ngay tao of this user nuoc sach.
	*
	* @param ngayTao the ngay tao of this user nuoc sach
	*/
	@Override
	public void setNgayTao(java.util.Date ngayTao) {
		_userNuocSach.setNgayTao(ngayTao);
	}

	/**
	* Returns the ngay sua of this user nuoc sach.
	*
	* @return the ngay sua of this user nuoc sach
	*/
	@Override
	public java.util.Date getNgaySua() {
		return _userNuocSach.getNgaySua();
	}

	/**
	* Sets the ngay sua of this user nuoc sach.
	*
	* @param ngaySua the ngay sua of this user nuoc sach
	*/
	@Override
	public void setNgaySua(java.util.Date ngaySua) {
		_userNuocSach.setNgaySua(ngaySua);
	}

	/**
	* Returns the nguoi tao of this user nuoc sach.
	*
	* @return the nguoi tao of this user nuoc sach
	*/
	@Override
	public java.lang.String getNguoiTao() {
		return _userNuocSach.getNguoiTao();
	}

	/**
	* Sets the nguoi tao of this user nuoc sach.
	*
	* @param nguoiTao the nguoi tao of this user nuoc sach
	*/
	@Override
	public void setNguoiTao(java.lang.String nguoiTao) {
		_userNuocSach.setNguoiTao(nguoiTao);
	}

	/**
	* Returns the nguoi sua of this user nuoc sach.
	*
	* @return the nguoi sua of this user nuoc sach
	*/
	@Override
	public java.lang.String getNguoiSua() {
		return _userNuocSach.getNguoiSua();
	}

	/**
	* Sets the nguoi sua of this user nuoc sach.
	*
	* @param nguoiSua the nguoi sua of this user nuoc sach
	*/
	@Override
	public void setNguoiSua(java.lang.String nguoiSua) {
		_userNuocSach.setNguoiSua(nguoiSua);
	}

	/**
	* Returns the to chuc ID of this user nuoc sach.
	*
	* @return the to chuc ID of this user nuoc sach
	*/
	@Override
	public long getToChucId() {
		return _userNuocSach.getToChucId();
	}

	/**
	* Sets the to chuc ID of this user nuoc sach.
	*
	* @param toChucId the to chuc ID of this user nuoc sach
	*/
	@Override
	public void setToChucId(long toChucId) {
		_userNuocSach.setToChucId(toChucId);
	}

	/**
	* Returns the screen name of this user nuoc sach.
	*
	* @return the screen name of this user nuoc sach
	*/
	@Override
	public java.lang.String getScreenName() {
		return _userNuocSach.getScreenName();
	}

	/**
	* Sets the screen name of this user nuoc sach.
	*
	* @param screenName the screen name of this user nuoc sach
	*/
	@Override
	public void setScreenName(java.lang.String screenName) {
		_userNuocSach.setScreenName(screenName);
	}

	/**
	* Returns the email address of this user nuoc sach.
	*
	* @return the email address of this user nuoc sach
	*/
	@Override
	public java.lang.String getEmailAddress() {
		return _userNuocSach.getEmailAddress();
	}

	/**
	* Sets the email address of this user nuoc sach.
	*
	* @param emailAddress the email address of this user nuoc sach
	*/
	@Override
	public void setEmailAddress(java.lang.String emailAddress) {
		_userNuocSach.setEmailAddress(emailAddress);
	}

	/**
	* Returns the ngay sinh of this user nuoc sach.
	*
	* @return the ngay sinh of this user nuoc sach
	*/
	@Override
	public java.util.Date getNgaySinh() {
		return _userNuocSach.getNgaySinh();
	}

	/**
	* Sets the ngay sinh of this user nuoc sach.
	*
	* @param ngaySinh the ngay sinh of this user nuoc sach
	*/
	@Override
	public void setNgaySinh(java.util.Date ngaySinh) {
		_userNuocSach.setNgaySinh(ngaySinh);
	}

	/**
	* Returns the gioi tinh of this user nuoc sach.
	*
	* @return the gioi tinh of this user nuoc sach
	*/
	@Override
	public boolean getGioiTinh() {
		return _userNuocSach.getGioiTinh();
	}

	/**
	* Returns <code>true</code> if this user nuoc sach is gioi tinh.
	*
	* @return <code>true</code> if this user nuoc sach is gioi tinh; <code>false</code> otherwise
	*/
	@Override
	public boolean isGioiTinh() {
		return _userNuocSach.isGioiTinh();
	}

	/**
	* Sets whether this user nuoc sach is gioi tinh.
	*
	* @param gioiTinh the gioi tinh of this user nuoc sach
	*/
	@Override
	public void setGioiTinh(boolean gioiTinh) {
		_userNuocSach.setGioiTinh(gioiTinh);
	}

	/**
	* Returns the mat khau of this user nuoc sach.
	*
	* @return the mat khau of this user nuoc sach
	*/
	@Override
	public java.lang.String getMatKhau() {
		return _userNuocSach.getMatKhau();
	}

	/**
	* Sets the mat khau of this user nuoc sach.
	*
	* @param matKhau the mat khau of this user nuoc sach
	*/
	@Override
	public void setMatKhau(java.lang.String matKhau) {
		_userNuocSach.setMatKhau(matKhau);
	}

	@Override
	public boolean isNew() {
		return _userNuocSach.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_userNuocSach.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _userNuocSach.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_userNuocSach.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _userNuocSach.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _userNuocSach.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_userNuocSach.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _userNuocSach.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_userNuocSach.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_userNuocSach.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_userNuocSach.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new UserNuocSachWrapper((UserNuocSach)_userNuocSach.clone());
	}

	@Override
	public int compareTo(UserNuocSach userNuocSach) {
		return _userNuocSach.compareTo(userNuocSach);
	}

	@Override
	public int hashCode() {
		return _userNuocSach.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<UserNuocSach> toCacheModel() {
		return _userNuocSach.toCacheModel();
	}

	@Override
	public UserNuocSach toEscapedModel() {
		return new UserNuocSachWrapper(_userNuocSach.toEscapedModel());
	}

	@Override
	public UserNuocSach toUnescapedModel() {
		return new UserNuocSachWrapper(_userNuocSach.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _userNuocSach.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _userNuocSach.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_userNuocSach.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserNuocSachWrapper)) {
			return false;
		}

		UserNuocSachWrapper userNuocSachWrapper = (UserNuocSachWrapper)obj;

		if (Validator.equals(_userNuocSach, userNuocSachWrapper._userNuocSach)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public UserNuocSach getWrappedUserNuocSach() {
		return _userNuocSach;
	}

	@Override
	public UserNuocSach getWrappedModel() {
		return _userNuocSach;
	}

	@Override
	public void resetOriginalValues() {
		_userNuocSach.resetOriginalValues();
	}

	private UserNuocSach _userNuocSach;
}