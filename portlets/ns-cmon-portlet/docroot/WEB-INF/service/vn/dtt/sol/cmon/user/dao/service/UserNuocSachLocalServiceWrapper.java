/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package vn.dtt.sol.cmon.user.dao.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link UserNuocSachLocalService}.
 *
 * @author khoa.vu
 * @see UserNuocSachLocalService
 * @generated
 */
public class UserNuocSachLocalServiceWrapper implements UserNuocSachLocalService,
	ServiceWrapper<UserNuocSachLocalService> {
	public UserNuocSachLocalServiceWrapper(
		UserNuocSachLocalService userNuocSachLocalService) {
		_userNuocSachLocalService = userNuocSachLocalService;
	}

	/**
	* Adds the user nuoc sach to the database. Also notifies the appropriate model listeners.
	*
	* @param userNuocSach the user nuoc sach
	* @return the user nuoc sach that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public vn.dtt.sol.cmon.user.dao.model.UserNuocSach addUserNuocSach(
		vn.dtt.sol.cmon.user.dao.model.UserNuocSach userNuocSach)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userNuocSachLocalService.addUserNuocSach(userNuocSach);
	}

	/**
	* Creates a new user nuoc sach with the primary key. Does not add the user nuoc sach to the database.
	*
	* @param userNuocSachPK the primary key for the new user nuoc sach
	* @return the new user nuoc sach
	*/
	@Override
	public vn.dtt.sol.cmon.user.dao.model.UserNuocSach createUserNuocSach(
		vn.dtt.sol.cmon.user.dao.service.persistence.UserNuocSachPK userNuocSachPK) {
		return _userNuocSachLocalService.createUserNuocSach(userNuocSachPK);
	}

	/**
	* Deletes the user nuoc sach with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userNuocSachPK the primary key of the user nuoc sach
	* @return the user nuoc sach that was removed
	* @throws PortalException if a user nuoc sach with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public vn.dtt.sol.cmon.user.dao.model.UserNuocSach deleteUserNuocSach(
		vn.dtt.sol.cmon.user.dao.service.persistence.UserNuocSachPK userNuocSachPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _userNuocSachLocalService.deleteUserNuocSach(userNuocSachPK);
	}

	/**
	* Deletes the user nuoc sach from the database. Also notifies the appropriate model listeners.
	*
	* @param userNuocSach the user nuoc sach
	* @return the user nuoc sach that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public vn.dtt.sol.cmon.user.dao.model.UserNuocSach deleteUserNuocSach(
		vn.dtt.sol.cmon.user.dao.model.UserNuocSach userNuocSach)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userNuocSachLocalService.deleteUserNuocSach(userNuocSach);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _userNuocSachLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userNuocSachLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link vn.dtt.sol.cmon.user.dao.model.impl.UserNuocSachModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _userNuocSachLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link vn.dtt.sol.cmon.user.dao.model.impl.UserNuocSachModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userNuocSachLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userNuocSachLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userNuocSachLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public vn.dtt.sol.cmon.user.dao.model.UserNuocSach fetchUserNuocSach(
		vn.dtt.sol.cmon.user.dao.service.persistence.UserNuocSachPK userNuocSachPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userNuocSachLocalService.fetchUserNuocSach(userNuocSachPK);
	}

	/**
	* Returns the user nuoc sach with the primary key.
	*
	* @param userNuocSachPK the primary key of the user nuoc sach
	* @return the user nuoc sach
	* @throws PortalException if a user nuoc sach with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public vn.dtt.sol.cmon.user.dao.model.UserNuocSach getUserNuocSach(
		vn.dtt.sol.cmon.user.dao.service.persistence.UserNuocSachPK userNuocSachPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _userNuocSachLocalService.getUserNuocSach(userNuocSachPK);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _userNuocSachLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the user nuoc sachs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link vn.dtt.sol.cmon.user.dao.model.impl.UserNuocSachModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user nuoc sachs
	* @param end the upper bound of the range of user nuoc sachs (not inclusive)
	* @return the range of user nuoc sachs
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<vn.dtt.sol.cmon.user.dao.model.UserNuocSach> getUserNuocSachs(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userNuocSachLocalService.getUserNuocSachs(start, end);
	}

	/**
	* Returns the number of user nuoc sachs.
	*
	* @return the number of user nuoc sachs
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getUserNuocSachsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userNuocSachLocalService.getUserNuocSachsCount();
	}

	/**
	* Updates the user nuoc sach in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param userNuocSach the user nuoc sach
	* @return the user nuoc sach that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public vn.dtt.sol.cmon.user.dao.model.UserNuocSach updateUserNuocSach(
		vn.dtt.sol.cmon.user.dao.model.UserNuocSach userNuocSach)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _userNuocSachLocalService.updateUserNuocSach(userNuocSach);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _userNuocSachLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_userNuocSachLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _userNuocSachLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public UserNuocSachLocalService getWrappedUserNuocSachLocalService() {
		return _userNuocSachLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedUserNuocSachLocalService(
		UserNuocSachLocalService userNuocSachLocalService) {
		_userNuocSachLocalService = userNuocSachLocalService;
	}

	@Override
	public UserNuocSachLocalService getWrappedService() {
		return _userNuocSachLocalService;
	}

	@Override
	public void setWrappedService(
		UserNuocSachLocalService userNuocSachLocalService) {
		_userNuocSachLocalService = userNuocSachLocalService;
	}

	private UserNuocSachLocalService _userNuocSachLocalService;
}