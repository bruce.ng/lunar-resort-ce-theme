package vn.dtt.ns.bean;

import java.sql.Date;

public class Kehoachkiemdem {
	private int id;
	private int kehoachid;
	private int daketthuc;
	private Date lastsave;

	public Kehoachkiemdem(int id, int kehoachid, int daketthuc, Date lastsave) {
		super();
		this.id = id;
		this.kehoachid = kehoachid;
		this.daketthuc = daketthuc;
		this.lastsave = lastsave;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public int getKehoachid() {
		return kehoachid;
	}

	public void setKehoachid(int kehoachid) {
		this.kehoachid = kehoachid;
	}
	public int getDaketthuc() {
		return daketthuc;
	}

	public void setDaketthuc(int daketthuc) {
		this.daketthuc = daketthuc;
	}
	public Date getLastsave() {
		return lastsave;
	}

	public void setLastsave(Date lastsave) {
		this.lastsave = lastsave;
	}

}
