/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package vn.dtt.ns.dao.service.impl;

import vn.dtt.ns.dao.service.base.DauNoiNuocWebserviceLocalServiceBaseImpl;

/**
 * The implementation of the dau noi nuoc webservice local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link vn.dtt.ns.dao.service.DauNoiNuocWebserviceLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author binhta
 * @see vn.dtt.ns.dao.service.base.DauNoiNuocWebserviceLocalServiceBaseImpl
 * @see vn.dtt.ns.dao.service.DauNoiNuocWebserviceLocalServiceUtil
 */
public class DauNoiNuocWebserviceLocalServiceImpl
	extends DauNoiNuocWebserviceLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link vn.dtt.ns.dao.service.DauNoiNuocWebserviceLocalServiceUtil} to access the dau noi nuoc webservice local service.
	 */
}