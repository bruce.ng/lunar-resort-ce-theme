package vn.dtt.ns.bean;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

public class ThongTinDauNoi {
	private Integer id;
	private Integer kehoachkiemdemid;
	private String daunoinuocid;
	private String matinh;
	private String mahuyen;
	private String maxa;
	private String thonxom;
	private String tenchuho;
	private Integer gioitinhchuho;
	private Integer songuoitrongho;
	private Integer thanhphanhogd;
	private Date thoigiandauhopdong;

	public ThongTinDauNoi(Integer id, Integer kehoachkiemdemid, String daunoinuocid, String matinh, String mahuyen,
			String maxa, String thonxom, String tenchuho, Integer gioitinhchuho, Integer songuoitrongho,
			Integer thanhphanhogd, Date thoigiandauhopdong) {
		super();
		this.id = id;
		this.kehoachkiemdemid = kehoachkiemdemid;
		this.daunoinuocid = daunoinuocid;
		this.matinh = matinh;
		this.maxa = maxa;
		this.mahuyen = mahuyen;
		this.thonxom = thonxom;
		this.tenchuho = tenchuho;
		this.gioitinhchuho = gioitinhchuho;
		this.songuoitrongho = songuoitrongho;
		this.thanhphanhogd = thanhphanhogd;
		this.thoigiandauhopdong = thoigiandauhopdong;

	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public int getKehoachkiemdemid() {
		return kehoachkiemdemid;
	}

	public void setKehoachkiemdemid(int kehoachkiemdemid) {
		this.kehoachkiemdemid = kehoachkiemdemid;
	}
	public String getDaunoinuocid() {
		return daunoinuocid;
	}

	public void setDaunoinuocid(String daunoinuocid) {
		this.daunoinuocid = daunoinuocid;
	}

	public String getMatinh() {
		return matinh;
	}

	public void setMatinh(String matinh) {
		this.matinh = matinh;
	}

	public String getMahuyen() {
		return mahuyen;
	}

	public void setMahuyen(String mahuyen) {
		this.mahuyen = mahuyen;
	}

	public String getMaxa() {
		return maxa;
	}

	public void setMaxa(String maxa) {
		this.maxa = maxa;
	}

	public String getThonxom() {
		return thonxom;
	}

	public void setThonxom(String thonxom) {
		this.thonxom = thonxom;
	}

	public String getTenchuho() {
		return tenchuho;
	}

	public void setTenchuho(String tenchuho) {
		this.tenchuho = tenchuho;
	}

	public Integer getGioitinhchuho() {
		return gioitinhchuho;
	}

	public void setGioitinhchuho(Integer gioitinhchuho) {
		this.gioitinhchuho = gioitinhchuho;
	}

	public Integer getSonguoitrongho() {
		return songuoitrongho;
	}

	public void setSonguoitrongho(Integer songuoitrongho) {
		this.songuoitrongho = songuoitrongho;
	}

	public Integer getThanhphanhogd() {
		return thanhphanhogd;
	}

	public void setThanhphanhogd(Integer thanhphanhogd) {
		this.thanhphanhogd = thanhphanhogd;
	}

	public Date getThoigiandauhopdong() {
		return thoigiandauhopdong;
	}

	public void setThoigiandauhopdong(Date thoigiandauhopdong) {
		this.thoigiandauhopdong = thoigiandauhopdong;
	}

}
