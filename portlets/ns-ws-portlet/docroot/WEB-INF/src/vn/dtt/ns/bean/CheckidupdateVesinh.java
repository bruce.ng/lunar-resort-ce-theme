package vn.dtt.ns.bean;

public class CheckidupdateVesinh {
	private int id_vesinh;
	private String id_mobile;
	private int id_kehoach;
	private int id_thongtinvs;

	public int getId_vesinh() {
		return id_vesinh;
	}

	public void setId_vesinh(int id_vesinh) {
		this.id_vesinh = id_vesinh;
	}

	public String getId_mobile() {
		return id_mobile;
	}

	public void setId_mobile(String id_mobile) {
		this.id_mobile = id_mobile;
	}

	public int getId_kehoach() {
		return id_kehoach;
	}

	public void setId_kehoach(int id_kehoach) {
		this.id_kehoach = id_kehoach;
	}

	public int getId_thongtinvs() {
		return id_thongtinvs;
	}

	public void setId_thongtinvs(int id_thongtinvs) {
		this.id_thongtinvs = id_thongtinvs;
	}

}
