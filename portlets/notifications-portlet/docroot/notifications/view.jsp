<%--
/**
* Copyright (c) 2000-2010 Liferay, Inc. All rights reserved.
*
* This library is free software; you can redistribute it and/or modify it under
* the terms of the GNU Lesser General Public License as published by the Free
* Software Foundation; either version 2.1 of the License, or (at your option)
* any later version.
*
* This library is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
* details.
*/
--%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.liferay.portal.kernel.json.JSONSerializer"%>
<%@page import="com.liferay.portal.kernel.json.JSONFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.json.JSONObject"%>
<%@page import="com.liferay.portal.model.UserNotificationEvent"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@page import="javax.portlet.ActionRequest"%>
<%@page import="com.liferay.portal.kernel.dao.orm.QueryUtil"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="com.liferay.portal.kernel.dao.search.SearchContainer"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Date"%>

<%@page pageEncoding="UTF-8"%>
<%@ include file="/init.jsp" %>

<%
SimpleDateFormat formatDateShort = new SimpleDateFormat("dd/MM/yyyy");
List<UserNotificationEvent> userNotificationEvents = UserNotificationEventLocalServiceUtil.getArchivedUserNotificationEvents(themeDisplay.getUserId(), true);

		List<UserNotificationEvent> userNotificationEvents1 = UserNotificationEventLocalServiceUtil.getArchivedUserNotificationEvents(themeDisplay.getUserId(), false);

		//System.out.println("----userNotificationEvents--"+userNotificationEvents.size());
		//System.out.println("----userNotificationEvents1--"+userNotificationEvents1.size());
		
	String currentUrl=themeDisplay.getURLCurrent();	
	long porletId=0;
	String porletName="";
		
	if (currentUrl.contains("/group/trang-user/")){
		porletId=11513;
		porletName="usertraodoithongtin_WAR_tvonlineportlet";
	}else if (currentUrl.contains("/group/trang-cua-chuyen-gia/")){
		porletId=11501;
		porletName="canbotuvan_WAR_tvonlineportlet";
	}
%>
<style >
.wd-select button {
	margin-top: 5px;
}

</style>

<script type = "text/javascript">
	function confirmDelete(){
		return confirm("Bạn thực sự muốn xóa thông báo này?");
	}
</script>

<portlet:actionURL var="timKiemNhomDoiTuong" name="timKiemNhomDoiTuong" >
</portlet:actionURL>


          	<liferay-portlet:renderURL plid="<%=porletId%>"  portletName="<%=porletName%>" var="EDITDON">
			</liferay-portlet:renderURL> 
  				<div class="wd-content-container">
				<h2 class="tlemenu"><b>THÔNG BÁO</b></h2>
				<div class="boxnd">
<!-- Ket qua tim kiem -->
<table class="wd-table">
    <thead>
        <tr>
             <th width="8%">STT</th>
             <th>Nội dung </th>
             <th  width="12%">Thời gian </th>
             <th width="20%">Thao tác</th>
        </tr>
    </thead>
    <tbody>
    <%
    	int countNhom=1;
    	if (userNotificationEvents!=null && userNotificationEvents.size()!=0){
    		
    		for (UserNotificationEvent item: userNotificationEvents){
    			
    			String urlDirect=EDITDON+"&_"+porletName+"_QUESTION_ID_NOTIFICATION="+item.getDeliverBy()+"&_"+porletName+"_USERNOTIFICATIONEVENTID="+item.getUserNotificationEventId();
    			///JsonElement jsonObject =new JsonParser().parse(item.getPayload());
    			//JSONObject jsonObject1 = JSONFactoryUtil.createJSONObject("{'name':'Hamidul Islam','country':'India'}");
    			JSONObject jsonObject1 = JSONFactoryUtil.createJSONObject(item.getPayload());
    			
				// Delete Url
				PortletURL deleteUrl = renderResponse.createActionURL();
				deleteUrl.setParameter(ActionRequest.ACTION_NAME, "deleteUserNotificationEvent");
				deleteUrl.setParameter("userNotificationEventId", String.valueOf(item.getUserNotificationEventId()));
				
				Date ngayGui=new Date(item.getTimestamp());
    			
    %>
    
        <tr>
             <td><%=countNhom++%></td>
             <td style="text-align: left;"><a href = "<%=urlDirect%>"><%=jsonObject1.getString("notificationText")%> </a></td>
             <td class="text-left"><%=formatDateShort.format(ngayGui) %></td>
             <td class="text-left">
									
                          									<a href = "<%=deleteUrl%>" onclick = "return confirmDelete();">
									<img src="<%=request.getContextPath()%>/xoa.png" title="Xóa thông báo">
									</a>									
             
             </td>
        </tr>
     <% } } %>   
     

    <%
    	if (userNotificationEvents1!=null && userNotificationEvents1.size()!=0){
    		for (UserNotificationEvent item: userNotificationEvents1){
    			
    			String urlDirect=EDITDON+"&_"+porletName+"_QUESTION_ID_NOTIFICATION="+item.getDeliverBy()+"&_"+porletName+"_USERNOTIFICATIONEVENTID="+item.getUserNotificationEventId();
    			///JsonElement jsonObject =new JsonParser().parse(item.getPayload());
    			//JSONObject jsonObject1 = JSONFactoryUtil.createJSONObject("{'name':'Hamidul Islam','country':'India'}");
    			JSONObject jsonObject1 = JSONFactoryUtil.createJSONObject(item.getPayload());
				// Delete Url
				PortletURL deleteUrl = renderResponse.createActionURL();
				deleteUrl.setParameter(ActionRequest.ACTION_NAME, "deleteUserNotificationEvent");
				deleteUrl.setParameter("userNotificationEventId", String.valueOf(item.getUserNotificationEventId()));
				
				// Delete Url
				PortletURL markAsReadUrl = renderResponse.createActionURL();
				markAsReadUrl.setParameter(ActionRequest.ACTION_NAME, "markAsRead");
				markAsReadUrl.setParameter("userNotificationEventId", String.valueOf(item.getUserNotificationEventId()));
				
				Date ngayGui=new Date(item.getTimestamp());
    %>

        <tr>
             <td><%=countNhom++ %></td>
             <td style="text-align: left;"><a href = "<%=urlDirect%>"><%=jsonObject1.getString("notificationText")%></a></td>
              <td class="text-left"><%=formatDateShort.format(ngayGui) %></td>
             <td class="text-left">
             
                          			<a href = "<%=deleteUrl%>" onclick = "return confirmDelete();">
										<img src="<%=request.getContextPath()%>/xoa.png" title="Xóa thông báo">
									</a>
									
									<a href = "<%=markAsReadUrl%>" >
									Đánh dấu đã đọc
									</a>
             
             </td>
        </tr>
     <% } } %>      
    </tbody>
</table>

</div></div>