<%--
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@ include file="/html/taglib/init.jsp" %>
<%@page pageEncoding="UTF-8"%>

<%
String formName = namespace + request.getAttribute("liferay-ui:page-iterator:formName");
int cur = GetterUtil.getInteger((String)request.getAttribute("liferay-ui:page-iterator:cur"));
String curParam = (String)request.getAttribute("liferay-ui:page-iterator:curParam");
int delta = GetterUtil.getInteger((String)request.getAttribute("liferay-ui:page-iterator:delta"));
boolean deltaConfigurable = GetterUtil.getBoolean((String)request.getAttribute("liferay-ui:page-iterator:deltaConfigurable"));
String deltaParam = (String)request.getAttribute("liferay-ui:page-iterator:deltaParam");
String jsCall = GetterUtil.getString((String)request.getAttribute("liferay-ui:page-iterator:jsCall"));
int maxPages = GetterUtil.getInteger((String)request.getAttribute("liferay-ui:page-iterator:maxPages"));
String target = (String)request.getAttribute("liferay-ui:page-iterator:target");
int total = GetterUtil.getInteger((String)request.getAttribute("liferay-ui:page-iterator:total"));
String type = (String)request.getAttribute("liferay-ui:page-iterator:type");
String url = (String)request.getAttribute("liferay-ui:page-iterator:url");
String urlAnchor = (String)request.getAttribute("liferay-ui:page-iterator:urlAnchor");
int pages = GetterUtil.getInteger((String)request.getAttribute("liferay-ui:page-iterator:pages"));

int start = (cur - 1) * delta;
int end = cur * delta;

if (end > total) {
	end = total;
}

int resultRowsSize = delta;

if (total < delta) {
	resultRowsSize = total;
}
else {
	resultRowsSize = total - ((cur - 1) * delta);

	if (resultRowsSize > delta) {
		resultRowsSize = delta;
	}
}

String deltaURL = HttpUtil.removeParameter(url, namespace + deltaParam);

NumberFormat numberFormat = NumberFormat.getNumberInstance(locale); 
%>

<div class="phantrang">
     <div class="tongso">
         <%
         if (total > delta) {
        	 int totalPages = 0;
             if (total % delta == 0) {
                 totalPages = total / delta;
             } else {
            	 totalPages = ((total - (total % delta)) / delta) + 1;
             }
        	 %>
        	 <p class="sohoso soright">Số hồ sơ hiển thị &nbsp;<span class="boder"><%= total %></span>Trang<span class="boder"><%=cur %></span>/<span class="boder"><%=totalPages %></span></p>
        	 <%
         } else {
         %>
         <p class="sohoso soright">Số hồ sơ hiển thị &nbsp;<span class="boder"><%= total %></span></p>
         <%
         }
         %>
         
     </div>
     <%
     if (total > delta) {
         %>
         <div class="sotrang">
         <%
         if (cur > 1) {
         %>
             <a href="<%=_getHREF(formName, curParam, 1, jsCall, url, urlAnchor) %>" class="top"></a>
             <a href="<%=_getHREF(formName, curParam, cur - 1, jsCall, url, urlAnchor) %>" class="back"></a>
          <%
         }
          int totalPages = 0;
          if (total % delta == 0) {
              totalPages = total / delta;
          } else {
        	  totalPages = ((total - (total % delta)) / delta) + 1;
          }

          boolean statusFirst = true;
          boolean statusLast = true;
          for (int i = 1; i <= totalPages; i++) {
             if (totalPages > 5) {
            	 if (i == cur) {
                     %>
                     <a href="<%=_getHREF(formName, curParam, i, jsCall, url, urlAnchor) %>" class="active_so"><%=i %></a>
                     <%
                 } else if ((i == (cur - 1)) || (i == (cur + 1))) {
                 %>
                     <a href="<%=_getHREF(formName, curParam, i, jsCall, url, urlAnchor) %>" class="so"><%=i %></a>
                 <%
                 } else if ((i < (cur - 1)) && statusFirst) {
                	 statusFirst = false;
                	 %>
                     <a href="#" class="so">...</a>
                 <%
                 } else if ((i > (cur + 1)) && statusLast) {
                	 statusLast = false;
                     %>
                     <a href="#" class="so">...</a>
                 <%
                 }
             } else {
            	 if (i == cur) {
                     %>
                     <a href="<%=_getHREF(formName, curParam, i, jsCall, url, urlAnchor) %>" class="active_so"><%=i %></a>
                     <%
                 } else {
                 %>
                     <a href="<%=_getHREF(formName, curParam, i, jsCall, url, urlAnchor) %>" class="so"><%=i %></a>
                 <%
                 }
             }
          }
          
          if (cur < totalPages) {
          %>
              <a href="<%=_getHREF(formName, curParam, cur + 1, jsCall, url, urlAnchor) %>" class="next"></a>
              <a href="<%=_getHREF(formName, curParam, totalPages, jsCall, url, urlAnchor) %>" class="end"></a>
          <%
          }
          %>
         </div>
         <%
     }
     %>
</div>
<br>
<br>
<%!
private String _getHREF(String formName, String curParam, int cur, String jsCall, String url, String urlAnchor) throws Exception {
	String href = null;

	if (Validator.isNotNull(url)) {
		href = HtmlUtil.escape(url + curParam + "=" + cur + urlAnchor);
	}
	else {
		href = "javascript:document." + formName + "." + curParam + ".value = '" + cur + "'; " + jsCall;
	}

	return href;
}
%>