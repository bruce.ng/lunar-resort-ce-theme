<%--
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="com.liferay.portal.kernel.servlet.SessionErrors"%>
<%@page import="com.liferay.portal.kernel.util.JavaConstants"%>
<%@page import="javax.portlet.PortletConfig"%>
<%@page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@page import="com.liferay.portal.model.Organization"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.model.User"%>
<%@ include file="/html/portlet/login/init.jsp" %>
<%@      page pageEncoding="UTF-8"%>

<%
String getTenDangNhap=request.getParameter("tenDangNhap");
String getPassDangNhap=request.getParameter("passWord");
System.out.println("\n =====getTenDangNhap------" +getTenDangNhap);

%>
<style>

.form {
width: 100%;
float: right;
overflow: inherit;
padding-top: 0px;
}


.aui h1 {
font-size: 24px;
}	

.portlet-msg-error {
  background: #fdd url(../images/messages/error.png) no-repeat 6px 50%;
  border: 1px solid #f00;
  display: block;
  margin: 2px auto 14px;
  padding: 2px 2px 2px 5px;
  text-align: left;
  font-weight: normal;
}
.fontcheck {
font-family: MyriadPro-Cond, Arial, Helvetica, sans-serif;
font-size: 16px;
color: #7c7c7c;
line-height: 30px;
}
.check_mk {
margin-right: 5px;
}
</style>

<portlet:defineObjects />
<liferay-theme:defineObjects />


<%
	//System.out.println("\n =====theme id------" + theme.getThemeId());
%>
<c:choose>
	<c:when test="<%= themeDisplay.isSignedIn() %>">

		<%

		String signedInAs = HtmlUtil.escape(user.getFullName());

		if (themeDisplay.isShowMyAccountIcon()) {
			signedInAs = "<a href=\"" + HtmlUtil.escape(themeDisplay.getURLMyAccount().toString()) + "\">" + signedInAs + "</a>";
		}
		String defaultMohUrl="";
		User _user = User.class.cast(user);
		if (_user != null) {
			List<Organization> organizations = _user.getOrganizations();
			if (organizations.size() > 0 && !_user.getEmailAddress().contains("test@liferay.com")) {
				defaultMohUrl = "/group" + organizations.get(0).getGroup().getFriendlyURL();
			}
		}
		%>
		
		<script type="text/javascript">
			var urlLength = '<%=defaultMohUrl.length()%>';
			if (0 < urlLength) {
				window.location = '<%=defaultMohUrl%>';
			}
		</script>
		<%= LanguageUtil.format(pageContext, "you-are-signed-in-as-x", signedInAs, false) %>
	</c:when>
	<c:otherwise>

		<%
		String redirect = ParamUtil.getString(request, "redirect");

		String login = LoginUtil.getLogin(request, "login", company);
		String password = StringPool.BLANK;
		boolean rememberMe = ParamUtil.getBoolean(request, "rememberMe");

		if (Validator.isNull(authType)) {
			authType = company.getAuthType();
		}
		%>

		<portlet:actionURL secure="<%= PropsValues.COMPANY_SECURITY_AUTH_REQUIRES_HTTPS || request.isSecure() %>" var="loginURL">
			<portlet:param name="saveLastPath" value="0" />
			<portlet:param name="struts_action" value="/login/login" />
			<portlet:param name="doActionAfterLogin" value="<%= portletName.equals(PortletKeys.FAST_LOGIN) ? Boolean.TRUE.toString() : Boolean.FALSE.toString() %>" />
		</portlet:actionURL>
		<aui:form action="<%= loginURL %>" method="post" name="fm" >
			<aui:input name="redirect" type="hidden" value="<%= redirect %>" />

			<c:choose>
				<c:when test='<%= SessionMessages.contains(request, "user_added") %>'>

					<%
					String userEmailAddress = (String)SessionMessages.get(request, "user_added");
					String userPassword = (String)SessionMessages.get(request, "user_added_password");
					%>

					<div class="portlet-msg-success">
						<c:choose>
							<c:when test="<%= company.isStrangersVerify() || Validator.isNull(userPassword) %>">
								<%= LanguageUtil.get(pageContext, "thank-you-for-creating-an-account") %>

								<c:if test="<%= company.isStrangersVerify() %>">
									<%= LanguageUtil.format(pageContext, "your-email-verification-code-has-been-sent-to-x", userEmailAddress) %>
								</c:if>
							</c:when>
							<c:otherwise>
								<%= LanguageUtil.format(pageContext, "thank-you-for-creating-an-account.-your-password-is-x", userPassword, false) %>
							</c:otherwise>
						</c:choose>

						<c:if test="<%= PrefsPropsUtil.getBoolean(company.getCompanyId(), PropsKeys.ADMIN_EMAIL_USER_ADDED_ENABLED) %>">
							<%= LanguageUtil.format(pageContext, "your-password-has-been-sent-to-x", userEmailAddress) %>
						</c:if>
					</div>
				</c:when>
				<c:when test='<%= SessionMessages.contains(request, "user_pending") %>'>

					<%
					String userEmailAddress = (String)SessionMessages.get(request, "user_pending");
					%>

					<div class="portlet-msg-success">
						<%= LanguageUtil.format(pageContext, "thank-you-for-creating-an-account.-you-will-be-notified-via-email-at-x-when-your-account-has-been-approved", userEmailAddress) %>
					</div>
				</c:when>
				
				
			</c:choose>

				<%
				String loginLabel = null;

				if (authType.equals(CompanyConstants.AUTH_TYPE_EA)) {
					loginLabel = "email-address";
				}
				else if (authType.equals(CompanyConstants.AUTH_TYPE_SN)) {
					loginLabel = "screen-name";
				}
				else if (authType.equals(CompanyConstants.AUTH_TYPE_ID)) {
					loginLabel = "id";
				}
				%>
					
					
					<div class="bg_login">
						<div class="form_login">
							<div class="bgtle_log">
								<h1 class="tlelogin">Đăng nhập<br><span>hệ thống</span></h1>
							</div>
							<div class="form">
								<p>					
					
					
			<%
				if (!ParamUtil.getString(request, "doActionAfterLogin").equalsIgnoreCase("false") ){
			%>
			
			<%} %>
			
			<liferay-ui:error exception="<%= AuthException.class %>" message="Xác thực không thành công, xin thử lại. " />
			<liferay-ui:error exception="<%= CompanyMaxUsersException.class %>" message="unable-to-login-because-the-maximum-number-of-users-has-been-reached" />
			<liferay-ui:error exception="<%= CookieNotSupportedException.class %>" message="authentication-failed-please-enable-browser-cookies" />
			<liferay-ui:error exception="<%= NoSuchUserException.class %>" message="authentication-failed" />
			<liferay-ui:error exception="<%= PasswordExpiredException.class %>" message="your-password-has-expired" />
			<liferay-ui:error exception="<%= UserEmailAddressException.class %>" message="authentication-failed" />
			<liferay-ui:error exception="<%= UserLockoutException.class %>" message="this-account-has-been-locked" />
			<liferay-ui:error exception="<%= UserPasswordException.class %>" message="authentication-failed" />
			<liferay-ui:error exception="<%= UserScreenNameException.class %>" message="authentication-failed" />

			<input class="userlogin" type="text" placeholder="Tài khoản" name="<portlet:namespace />login" id="<portlet:namespace />login" size="20" onfocus="myFocus(this);"
				 >
			<input class="userlogin" type="password" placeholder="Mật khẩu" name="<portlet:namespace />password" id="<portlet:namespace />password"   size="20"  onfocus="myFocus(this);"
				 >
			 </div>
							<div class="in">
								<p class="fontcheck">
									<input class="check_mk" type="checkbox">Nhớ mật khẩu 
									<input class="submit" type="submit" value="Đăng nhập"/>
								</p>
							</div>
						
						</div>
					</div>			
		</aui:form>

		<c:if test="<%= windowState.equals(WindowState.MAXIMIZED) %>">
			<aui:script>
				Liferay.Util.focusFormField(document.<portlet:namespace />fm.<portlet:namespace />login);
			</aui:script>
		</c:if>

		<aui:script use="aui-base">
			var password = A.one('#<portlet:namespace />password');

			if (password) {
				password.on(
					'keypress',
					function(event) {
						Liferay.Util.showCapsLock(event, '<portlet:namespace />passwordCapsLockSpan');
					}
				);
			}
		</aui:script>
	</c:otherwise>
</c:choose>
<script type="text/javascript">	

	var homeThemeId = '<%= theme.getThemeId() %>';
	function mySubmit() {
		document.getElementById("<portlet:namespace />login").value =	document.getElementById("<portlet:namespace />tenDangNhap").value;
	}
	
	function myFocus(divObj) {
		if (homeThemeId != 'mohboytehome2_WAR_mohboytehome2theme') {
			var arr = document.getElementsByTagName('input');
			for(var i = 0; i < arr.length; i++)
		    {
				arr[i].style.border = '';
				arr[i].style.boxShadow = '';
			}
			divObj.style.border = '1px solid #5AAE39';
			divObj.style.boxShadow = '0 1px 1px rgba(0, 0, 0, 0.05) inset, 0 0 3px #5AAE39';
		}
	}
	
	var tableLogin = document.getElementById('tableLogin');

	var headerButtons = document.getElementsByClassName('ctrl-header')[0];
	if (headerButtons) {
		headerButtons.children[0].style.display = 'none';
	}

	
</script>
