<%--
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@page import="com.liferay.portal.model.Organization"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.model.User"%>
<%@ include file="/html/portlet/login/init.jsp" %>
<%@      page pageEncoding="UTF-8"%>


<style>
.clum_left1 a {
float: left;
padding: 0px;
width: 27%;
border-radius: 0px;
border: solid 0px #cacaca;
margin-right: 0px;
margin-top: 15px;
text-align: center;
font-size: 18px;
text-transform: uppercase;
color: #194284;
height: 40px;
}
	input:focus
	{
		background-color:white;
		border: 1px solid #53B9FE;
		box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05) inset, 0 0 6px #99CCFF;
	}
	
	h2 {
	    color: #5E5E5E;
	    cursor: pointer;
	    font-size: 24px;
	    font-weight: bold;
	    margin-bottom: 30px;
	    text-decoration: none;
	    text-transform: uppercase;
	    padding-left: 115px;
	}
	#login-bt {
	    background: #262D7A;
	    border: medium none;
	    color: #FFFFFF;
	    font-family: arial;
	    font-size: 12px;
	    font-weight: bold;
	    height: 25px;
	    text-decoration: none;
	    width: 100px;
	    text-shadow:none
	}
	#login-bt:hover {
	   background-color: #5F78AB;
	}
	
	
	body { 
background-size:100%; 
}
	table tr td, table tr th {
border: solid 0px #c9c9c9;
padding: 6px 12px;
}
</style>
<portlet:defineObjects />
<liferay-theme:defineObjects />


<%
	//System.out.println("\n =====theme id------" + theme.getThemeId());

%>

<c:choose>
	<c:when test="<%= themeDisplay.isSignedIn() %>">

		<%
		
		String signedInAs = HtmlUtil.escape(user.getFullName());

		if (themeDisplay.isShowMyAccountIcon()) {
			signedInAs = "<a href=\"" + HtmlUtil.escape(themeDisplay.getURLMyAccount().toString()) + "\">" + signedInAs + "</a>";
		}
		String defaultMohUrl="";
		User _user = User.class.cast(user);
		if (_user != null) {
			List<Organization> organizations = _user.getOrganizations();
			if (organizations.size() > 0 && !_user.getEmailAddress().contains("test@liferay.com")) {
				defaultMohUrl = "/group" + organizations.get(0).getGroup().getFriendlyURL();
			}
		}
		%>
		
		<script type="text/javascript">
			var urlLength = '<%=defaultMohUrl.length()%>';
			if (0 < urlLength) {
				window.location = '<%=defaultMohUrl%>';
			}
		</script>
		<%= LanguageUtil.format(pageContext, "you-are-signed-in-as-x", signedInAs, false) %>
	</c:when>
	<c:otherwise>

		<%
		String redirect = ParamUtil.getString(request, "redirect");

		String login = LoginUtil.getLogin(request, "login", company);
		String password = StringPool.BLANK;
		boolean rememberMe = ParamUtil.getBoolean(request, "rememberMe");

		if (Validator.isNull(authType)) {
			authType = company.getAuthType();
		}
		%>

		<portlet:actionURL secure="<%= PropsValues.COMPANY_SECURITY_AUTH_REQUIRES_HTTPS || request.isSecure() %>" var="loginURL">
			<portlet:param name="saveLastPath" value="0" />
			<portlet:param name="struts_action" value="/login/login" />
			<portlet:param name="doActionAfterLogin" value="<%= portletName.equals(PortletKeys.FAST_LOGIN) ? Boolean.TRUE.toString() : Boolean.FALSE.toString() %>" />
		</portlet:actionURL>
		<aui:form action="<%= loginURL %>" method="post" name="fm" >
			<aui:input name="redirect" type="hidden" value="<%= redirect %>" />

			<c:choose>
				<c:when test='<%= SessionMessages.contains(request, "user_added") %>'>

					<%
					String userEmailAddress = (String)SessionMessages.get(request, "user_added");
					String userPassword = (String)SessionMessages.get(request, "user_added_password");
					%>

					<div class="portlet-msg-success">
						<c:choose>
							<c:when test="<%= company.isStrangersVerify() || Validator.isNull(userPassword) %>">
								<%= LanguageUtil.get(pageContext, "thank-you-for-creating-an-account") %>

								<c:if test="<%= company.isStrangersVerify() %>">
									<%= LanguageUtil.format(pageContext, "your-email-verification-code-has-been-sent-to-x", userEmailAddress) %>
								</c:if>
							</c:when>
							<c:otherwise>
								<%= LanguageUtil.format(pageContext, "thank-you-for-creating-an-account.-your-password-is-x", userPassword, false) %>
							</c:otherwise>
						</c:choose>

						<c:if test="<%= PrefsPropsUtil.getBoolean(company.getCompanyId(), PropsKeys.ADMIN_EMAIL_USER_ADDED_ENABLED) %>">
							<%= LanguageUtil.format(pageContext, "your-password-has-been-sent-to-x", userEmailAddress) %>
						</c:if>
					</div>
				</c:when>
				<c:when test='<%= SessionMessages.contains(request, "user_pending") %>'>

					<%
					String userEmailAddress = (String)SessionMessages.get(request, "user_pending");
					%>

					<div class="portlet-msg-success">
						<%= LanguageUtil.format(pageContext, "thank-you-for-creating-an-account.-you-will-be-notified-via-email-at-x-when-your-account-has-been-approved", userEmailAddress) %>
					</div>
				</c:when>
			</c:choose>

			<liferay-ui:error exception="<%= AuthException.class %>" message="authentication-failed" />
			<liferay-ui:error exception="<%= CompanyMaxUsersException.class %>" message="unable-to-login-because-the-maximum-number-of-users-has-been-reached" />
			<liferay-ui:error exception="<%= CookieNotSupportedException.class %>" message="authentication-failed-please-enable-browser-cookies" />
			<liferay-ui:error exception="<%= NoSuchUserException.class %>" message="authentication-failed" />
			<liferay-ui:error exception="<%= PasswordExpiredException.class %>" message="your-password-has-expired" />
			<liferay-ui:error exception="<%= UserEmailAddressException.class %>" message="authentication-failed" />
			<liferay-ui:error exception="<%= UserLockoutException.class %>" message="this-account-has-been-locked" />
			<liferay-ui:error exception="<%= UserPasswordException.class %>" message="authentication-failed" />
			<liferay-ui:error exception="<%= UserScreenNameException.class %>" message="authentication-failed" />

			

				<%
				String loginLabel = null;

				if (authType.equals(CompanyConstants.AUTH_TYPE_EA)) {
					loginLabel = "email-address";
				}
				else if (authType.equals(CompanyConstants.AUTH_TYPE_SN)) {
					loginLabel = "screen-name";
				}
				else if (authType.equals(CompanyConstants.AUTH_TYPE_ID)) {
					loginLabel = "id";
				}
				%>
			<div class="boxpage">			
				<table class="noboder" border="0" cellpadding="0" style="border-collapse: collapse" width="100%">
					<tr>
						<td width="30%"></td>
						<td width="10%"></td>
						<td width="60%"></td>
					</tr>
					<tr>
						<td></td>
						<td style="height: 48px;padding-top: 0px;" colspan="2">
							<h2> ĐĂNG NHẬP </h2>
						</td>
					</tr>
					<tr>
						<td></td>
						<td  style="padding-bottom: 12px;">
							<label style="font-weight: bold; font-size: 12px;"> Tên đăng nhập : </label>
						</td>
						<td>
							<input type="hidden" value="<%= login %>" name="<portlet:namespace />login" id="<portlet:namespace />login" >
								
							<input type="text" value="<%= login %>" name="<portlet:namespace />tenDangNhap" id="<portlet:namespace />tenDangNhap" onfocus="myFocus(this);" 
								style="font-size: 12px;padding-left:10px;height: 20px;margin-bottom: 15px;width: 250px;" aria-required="true">
									
						</td>
					</tr>
					<tr>
						<td></td>
						<td  style="padding-bottom: 12px;">
							<label  style="font-size: 12px;font-weight: bold;"> Mật khẩu : </label>
						</td>
						<td>
							<input type="password" value="" name="<portlet:namespace />password" id="<portlet:namespace />password"  onfocus="myFocus(this);" 
								style="padding-left:10px;height: 20px;margin-bottom: 15px;width: 250px;"  aria-required="true">
						</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td>
							<span id="<portlet:namespace />passwordCapsLockSpan" style="display: none;">Bạn đang bật CapsLk</span>
						</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td>
							<c:if test="<%= company.isAutoLogin() && !PropsValues.SESSION_DISABLED %>">
								<span class="aui-field aui-field-choice">
									<span class="aui-field-content" style="margin-bottom: 15px;">
										<label for="<portlet:namespace />rememberMeCheckbox" class="aui-field-label"> Ghi nhớ </label> 
										<span class="aui-field-element aui-field-label-right">
											<input type="hidden" value="false" name="<portlet:namespace />rememberMe" id="<portlet:namespace />rememberMe"> 
											<input type="checkbox" value="true" onclick="Liferay.Util.updateCheckboxValue(this); " name="<portlet:namespace />rememberMeCheckbox"
											 id="<portlet:namespace />rememberMeCheckbox" class="aui-field-input aui-field-input-choice"> 
										</span> 
									</span>
								</span>
							</c:if>
						</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td>
							<input type="submit" value="Đăng nhập" id="login-bt" onclick="mySubmit()">
						</td>
					</tr>
					<tr>
                        <td></td>
                        <td></td>
                        <td>
                        </td>
                    </tr>
				</table>	
			</div>
		</aui:form>

		<c:if test="<%= windowState.equals(WindowState.MAXIMIZED) %>">
			<aui:script>
				Liferay.Util.focusFormField(document.<portlet:namespace />fm.<portlet:namespace />login);
			</aui:script>
		</c:if>

		<aui:script use="aui-base">
			var password = A.one('#<portlet:namespace />password');

			if (password) {
				password.on(
					'keypress',
					function(event) {
						Liferay.Util.showCapsLock(event, '<portlet:namespace />passwordCapsLockSpan');
					}
				);
			}
		</aui:script>
	</c:otherwise>
</c:choose>
<script type="text/javascript">	

	var homeThemeId = '<%= theme.getThemeId() %>';
	function mySubmit() {
		document.getElementById("<portlet:namespace />login").value =	document.getElementById("<portlet:namespace />tenDangNhap").value;
	}
	
	function myFocus(divObj) {
		if (homeThemeId != 'mohboytehome2_WAR_mohboytehome2theme') {
			var arr = document.getElementsByTagName('input');
			for(var i = 0; i < arr.length; i++)
		    {
				arr[i].style.border = '';
				arr[i].style.boxShadow = '';
			}
			divObj.style.border = '1px solid #5AAE39';
			divObj.style.boxShadow = '0 1px 1px rgba(0, 0, 0, 0.05) inset, 0 0 3px #5AAE39';
		}
	}
	
	var tableLogin = document.getElementById('tableLogin');
	if (tableLogin) {
		if (homeThemeId == 'mohboytehome2_WAR_mohboytehome2theme'  ||  homeThemeId =='mohhomeantoanthucpham_version22_WAR_mohhomeantoanthucpham_version22theme') {

			tableLogin.style.background = "url('/html/portlet/login/images/bg_login.jpg') repeat-x  ";
			tableLogin.style.backgroundSize ='100%';
			
			var loginDiv = document.getElementById('loginDiv');
			loginDiv.style.marginBottom = '3px';
			loginDiv.style.marginTop = '-5px';

			var loginBtn = document.getElementById('login-bt');
			loginBtn.style.height = '26px';
			loginBtn.style.borderRadius = '0';
		} else {
			tableLogin.style.background = "";
			document.getElementById('login-bt').style.background = '#2D8724';

			var loginDiv = document.getElementById('loginDiv');
			loginDiv.style.borderColor = '#E5E5E5';
			loginDiv.style.borderStyle = 'solid';
			loginDiv.style.borderWidth = '1px 1px 5px';
			loginDiv.style.height = '390px';
			loginDiv.style.marginBottom = '-21px';
			loginDiv.style.width = '997px';
		}
	}
	var headerButtons = document.getElementsByClassName('ctrl-header')[0];
	if (headerButtons) {
		headerButtons.children[0].style.display = 'none';
	}

	
</script>
